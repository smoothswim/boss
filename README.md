
# BOSS Music Player

BOSS Music Player is a cross platform Music Player application primarily developed for Windows 10 (64 bit)

![screenshot](/web/content/assets/images/screencsp.png)

## Downloads

Copies of the program for Windows 10 are available from the [project's home page](https://smoothswim.gitlab.io/boss/).

## Documentation

A user guide is available [here](https://gitlab.com/smoothswim/boss/-/blob/master/docs/src/User%20guide.pdf).

## Building

The application uses a modified number of [Tabler icons][9e67263f] and compiles against the [Qt][ab73d475] C++ SDK.

  [9e67263f]: https://github.com/tabler/tabler-icons/ "Tabler icons"
  [ab73d475]: https://www.qt.io/ "Qt C++ SDK"

Building the application on Windows 10 has the following dependencies:

- [Cmake](https://cmake.org) (_version 3.17 minimum_)
- [Microsoft Visual Studio](https://visualstudio.microsoft.com) (_version 2019_) and the following individual components:
  - **MsBuild**
  - **MSVC** (_v142 VS 2019 C++ x64/x86 build tools (v14.25) or newer_)
  - **MSVC** (_v142 VS 2019 C++ x64/x86 Spectre-mitigated libs v14.25 or newer_)
  - **C++ ATL** for latest V142 build tools (_x86 & x64_)
  - **C++ ATL** for latest V142 build tools with spectre mitigations (_x86 & x64_)
  - **C++ MFC** for latest V142 build tools (_x86 & x64_)
  - **C++ MFC** for latest V142 build tools with spectre mitigations (_x86 & x64_)
  - **C++ Core features**
  - **Windows 10 SDK** (_latest - current 10.0.18362.0_)
  - **Windows Universal C runtime**
  - **[Git](https://git-scm.com) for Windows** (_required only if git is not already installed_)

You can use the file **windows\backup.vsconfig** and the instructions found [here][d4d774a1] to easily add the required components to Visual Studio.

  [d4d774a1]: https://docs.microsoft.com/en-us/visualstudio/install/import-export-installation-configurations?view=vs-2019#import-a-configuration "vs config import"

Once you have the build dependencies installed, you can copy the file **windows\vars.cmd** to the file **windows\myvars.cmd** and once the correct locations within variables have been set inside **myvars.cmd**, the file can be run from the command prompt to begin the build. The very first build requires a working Internet connection. Subsequent re-builds do not and you can use **myvars.cmd** as many times as you like to rebuild the application. The script is an easy and convenient way to trigger a re-build if you make lots of changes.

If you find a bug, please report it [here][eb18056a].

**Thanks!**

  [eb18056a]: https://gitlab.com/smoothswim/boss/issues "issue tracker"
