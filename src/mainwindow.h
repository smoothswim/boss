/*

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QStandardItemModel>
#include <QSystemTrayIcon>
#include <QRadioButton>
#include <QMenu>
#include <QKeyEvent>
#include <QLineEdit>
#include <QStringList>
#include <QFileDialog>
#include <QDesktopServices>
#include <QSettings>
#include <QColorDialog>
#include <QLabel>
#include <QTranslator>
#include <QIcon>
#ifdef Q_OS_WIN
    #include <QtWin>
#endif

using namespace std;

#ifdef Q_OS_ANDROID

#define ROUTEICO "assets:/images/route-grey.png"
#define REPEATICO "assets:/images/repeat-grey.png"
#define PLAYICO "assets:/images/caret-right-grey.png"
#define RWICO "assets:/images/chevrons-left-grey.png"
#define FFICO "assets:/images/chevrons-right-grey.png"
#define MUTEICO "assets:/images/volume-3-grey.png"
#define SAVEICO "assets:/images/device-floppy-grey.png"
#define REMOVEICO "assets:/images/minus-grey.png"
#define ADDICO "assets:/images/plus-grey.png"
#define EMPTYICO "assets:/images/x-grey.png"
#define CONFIGICO "assets:/images/dots-grey.png"
#define ABOUTICO "assets:/images/info-circle-grey.png"
#define APPICO "assets:/images/app.png"
#define PAUSEICO "assets:/images/pause-grey.png"
#define VOLICO "assets:/images/volume-grey.png"
#define WMUTEICO "assets:/images/volume-3-white.png"
#define WVOLICO "assets:/images/volume-white.png"
#define WPHONESICO "assets:/images/headphones-white.png"
#define WCLEFICO "assets:/images/music-white.png"
#define BVOLICO "assets:/images/volume.png"
#define BMUTEICO "assets:/images/volume-3.png"
#define TRAYQUITICO "assets:/images/power.png"
#define TRAYMUTEICO "assets:/images/volume-3.png"
#define TRAYPLAYICO "assets:/images/caret-right.png"
#define TRAYRWICO "assets:/images/chevrons-left.png"
#define TRAYFFICO "assets:/images/chevrons-right.png"
#define TRAYPAUSEICO "assets:/images/pause.png"

#else

#define ROUTEICO ":/resources/route-grey.svg"
#define REPEATICO ":/resources/repeat-grey.svg"
#define PLAYICO ":/resources/caret-right-grey.svg"
#define RWICO ":/resources/chevrons-left-grey.svg"
#define FFICO ":/resources/chevrons-right-grey.svg"
#define MUTEICO ":/resources/volume-3-grey.svg"
#define SAVEICO ":/resources/device-floppy-grey.svg"
#define REMOVEICO ":/resources/minus-grey.svg"
#define ADDICO ":/resources/plus-grey.svg"
#define EMPTYICO ":/resources/x-grey.svg"
#define CONFIGICO ":/resources/dots-grey.svg"
#define ABOUTICO ":/resources/info-circle-grey.svg"
#define APPICO ":/resources/app.svg"
#define PAUSEICO ":/resources/pause-grey.svg"
#define VOLICO ":/resources/volume-grey.svg"
#define WMUTEICO ":/resources/volume-3-white.svg"
#define WVOLICO ":/resources/volume-white.svg"
#define WPHONESICO ":/resources/headphones-white.svg"
#define WCLEFICO ":/resources/music-white.svg"
#define BVOLICO ":/resources/volume.svg"
#define BMUTEICO ":/resources/volume-3.svg"
#define TRAYQUITICO ":/resources/power.svg"
#define TRAYMUTEICO ":/resources/volume-3.svg"
#define TRAYPLAYICO ":/resources/caret-right.svg"
#define TRAYRWICO ":/resources/chevrons-left.svg"
#define TRAYFFICO ":/resources/chevrons-right.svg"
#define TRAYPAUSEICO ":/resources/pause.svg"

#endif

namespace Ui {

    class MainWindow;

}

class MainWindow : public QMainWindow {

    Q_OBJECT

    public:

        explicit MainWindow(QWidget *parent = 0);
        virtual ~MainWindow();

    private slots:

        void on_searchBar_textChanged(const QString &arg1);
        bool on_add_clicked();
        bool on_remove_clicked();
        bool on_save_clicked();
        bool on_empty_clicked();
        bool on_config_clicked();
        bool on_about_clicked();
        bool on_play_clicked();
        bool on_volumeBar_valueChanged(int value);
        bool on_seekBar_sliderPressed();
        bool on_seekBar_sliderReleased();
        bool on_tableView_doubleClicked(const QModelIndex &index);
        bool on_forward_clicked();
        bool on_back_clicked();
        bool on_mute_clicked();
        bool on_repeat_clicked();
        bool on_shuffle_clicked();
        bool iconActivated(QSystemTrayIcon::ActivationReason reason);
        bool configClose_released(bool exiting);
        bool durationChanged(qint64 duration);
        bool positionChanged(qint64 progress);
        bool mutedChanged();
        bool dataModelUpdate(QStandardItem *item);
        bool seek(int seconds);
        bool getColour1();
        bool getColour2();
        bool getColour3();
        bool getColour4();
        bool getColour5();
        bool colourButtons();
        bool themeColourReset();

    private:

        int thisIndex = 0;
        int lastIndex = 0;
        bool wav = false;
        bool flac = false;
        bool ogg = false;
        bool mp3 = false;
        bool wma = false;
        bool opus = false;
        bool weba = false;
        bool spx = false;
        bool aac = false;
        bool m4a = false;
        bool config = false;
        Ui::MainWindow *ui;
        QMediaPlayer *player = new QMediaPlayer();
        QMediaPlaylist *playListObj = new QMediaPlaylist(player);
        QStandardItemModel *playListDataModel = new QStandardItemModel();
        QIcon appIco;
        QMenu *trayMenu;
        qint64 maxDuration;
        QLabel *configLabel;
        QSystemTrayIcon *trayIcon;
        QAction *quitAction;
        QAction *muteButtonAction;
        QAction *playButtonAction;
        QAction *backButtonAction;
        QAction *forwardButtonAction;
        QStringList filters;
        QStringList files;
        QColor newColour1;
        QColor newColour2;
        QColor newColour3;
        QColor newColour4;
        QColor newColour5;
        QColor currentColour1;
        QColor currentColour2;
        QColor currentColour3;
        QColor currentColour4;
        QColor currentColour5;
        QRadioButton *radio1;
        QRadioButton *radio2;
        QRadioButton *radio3;
        QRadioButton *radio4;
        QRadioButton *radio5;
        QRadioButton *radio6;
        QString appStyle;
        QString nativeTheme;
        QString escapeKeyExit;
        QString messagePreTrim;
        QString messageTextNew;
        QString messageTextTemp;
        QString settingTrayicon;
        QString settingAutoplay;
        QString currentPlaylistFile = "none.m3u";
        QString loadPlaylistsAtStart;
        QString resumePlayback;
        QString lastPlayed;
        QString playOption = "";
        QPushButton *colorButton1;
        QPushButton *colorButton2;
        QPushButton *colorButton3;
        QPushButton *colorButton4;
        QPushButton *colorButton5;
        QPushButton *colorButton6;

    protected:

        virtual QString trimFileExt(QString fileName);
        virtual void keyPressEvent(QKeyEvent *event);
        virtual void closeEvent(QCloseEvent *event);
        virtual bool eventFilter(QObject *obj, QEvent *event);
        virtual bool dumpPlaylistData();
        virtual bool dumpModelData();
        virtual bool forward();
        virtual bool back();
        virtual bool singleFile(QString line);
        virtual bool loadPlaylistFile(QString m3uPlaylistFile);
        virtual bool writePlaylistFile(QString currentPlaylistFile);
        virtual bool fileExists(QString path);
        virtual bool playNow();
        virtual bool loadSettings();
        virtual bool themeStyles();
        virtual bool restoreLast();
        virtual bool playIconPause();
        virtual bool playIconPlay();
        virtual int getIndex();

};

#endif // MAINWINDOW_H
