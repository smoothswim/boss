/*

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

*/

#include <QApplication>
#include <QTextCodec>

#include "mainwindow.h"

int main(int argc, char *argv[]) {

    QTextCodec::setCodecForLocale(QTextCodec::codecForName("utf-8"));

    QApplication::setAttribute(Qt::AA_UseHighDpiPixmaps); // https://doc.qt.io/qt-5/qt.html#ApplicationAttribute-enum
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication a(argc, argv);

    QTranslator bossTranslator; // https://doc.qt.io/qt-5/internationalization.html
    bossTranslator.load("boss_" + QLocale::system().name());
    a.installTranslator(&bossTranslator);

    MainWindow w;
    //w.setWindowFlags(Qt::FramelessWindowHint); // Borderless window | https://doc.qt.io/qt-5/qt.html#WindowType-enum
    w.show();

    return a.exec();

}
