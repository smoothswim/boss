/*

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

*/

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {

    #ifndef Q_OS_ANDROID
        Q_INIT_RESOURCE(icons); // https://doc.qt.io/qt-5/resources.html
    #endif
    ui->setupUi(this);
    this->setFixedSize(this->geometry().width(),this->geometry().height());

    loadSettings();

    themeStyles();

    ui->shuffle->setIcon(QIcon(ROUTEICO));
    ui->repeat->setIcon(QIcon(REPEATICO));
    ui->play->setIcon(QIcon(PLAYICO));
    ui->back->setIcon(QIcon(RWICO));
    ui->forward->setIcon(QIcon(FFICO));
    ui->mute->setIcon(QIcon(MUTEICO));
    ui->save->setIcon(QIcon(SAVEICO));
    ui->remove->setIcon(QIcon(REMOVEICO));
    ui->add->setIcon(QIcon(ADDICO));
    ui->empty->setIcon(QIcon(EMPTYICO));
    ui->config->setIcon(QIcon(CONFIGICO));
    ui->about->setIcon(QIcon(ABOUTICO));

    appIco = QIcon(APPICO);
    setWindowIcon(appIco);

    setWindowTitle("BOSS Music Player");

    player->setAudioRole(QAudio::MusicRole); // https://doc.qt.io/qt-5/qaudio.html#Role-enum
    player->setPlaylist(playListObj);

    connect(player, QOverload<QMediaPlayer::Error>::of(&QMediaPlayer::error), [=](QMediaPlayer::Error error){

        player->pause();
        on_remove_clicked();
        ui->currentSong->setText(tr("Playback Error: Please try to re-encode the file"));

        if (playListObj->mediaCount() > 0) {

            player->play();
            playIconPlay();

        } else {

            playIconPause();

        }

    });

    playListDataModel->setHorizontalHeaderLabels(QStringList() << tr("Track") << tr("Location"));
    playListDataModel->setColumnCount(1);
    playListDataModel->setRowCount(0);
    ui->verticalLayout->setAlignment(Qt::AlignCenter);
    ui->tableView->setModel(playListDataModel);
    ui->tableView->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);
    ui->tableView->horizontalHeader()->setStretchLastSection(true);
    ui->tableView->horizontalHeader()->setSectionsMovable(false);
    ui->tableView->horizontalHeader()->setVisible(false);
    ui->tableView->verticalHeader()->setVisible(false);
    ui->tableView->verticalHeader()->setSectionsMovable(false);
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableView->setDragEnabled(true);
    ui->tableView->viewport()->setAcceptDrops(false);
    ui->tableView->setDefaultDropAction(Qt::MoveAction);
    ui->tableView->setDropIndicatorShown(true);
    ui->tableView->setDragDropMode(QAbstractItemView::DragDrop);
    ui->tableView->setDragDropOverwriteMode(false);
    ui->tableView->setSortingEnabled(false);

    connect(playListDataModel, &QStandardItemModel::itemChanged, this, &MainWindow::dataModelUpdate);

    connect(ui->tableView, &QTableView::doubleClicked, [this](const QModelIndex &index) {

        ui->tableView->selectionModel()->clearSelection();
        ui->tableView->selectionModel()->setCurrentIndex(index, QItemSelectionModel::Select);
        playListObj->setCurrentIndex(index.row());

    });

    connect(playListObj, &QMediaPlaylist::currentIndexChanged, [this](int index) {

        const int num = playListObj->mediaCount();

        if (num >= 1) {

            const QString cT = playListObj->media(playListObj->currentIndex()).request().url().toString();
            const QFile fileA(QUrl(playListObj->media(playListObj->currentIndex()).request().url()).fileName());
            const QFileInfo fileInfo(fileA);
            const QString currentTrackName = trimFileExt(fileInfo.fileName());

            ui->currentSong->setText(currentTrackName);
            ui->tableView->selectRow(index);

        }

        lastIndex = thisIndex;
        thisIndex = index;

    });

    connect(player, &QMediaPlayer::mutedChanged, this, &MainWindow::mutedChanged);
    connect(player, &QMediaPlayer::durationChanged, this, &MainWindow::durationChanged);
    connect(player, &QMediaPlayer::positionChanged, this, &MainWindow::positionChanged);
    connect(ui->seekBar, &QSlider::sliderMoved, this, &MainWindow::seek);

    connect(playListObj, &QMediaPlaylist::playbackModeChanged, [this]() {

        // https://doc.qt.io/qt-5/qmediaplaylist.html#PlaybackMode-enum

        QSettings settings("BOSS", "Player");

        switch (playListObj->playbackMode()) {

            case QMediaPlaylist::CurrentItemOnce :

                if (playListObj->mediaCount() > 1) {

                    ui->repeat->setChecked(true);
                    settings.setValue("playback/repeat", "yes");
                    ui->shuffle->setChecked(false);
                    settings.setValue("playback/shuffle", "no");

                } else {

                    if (ui->repeat->isChecked() == Qt::Unchecked) {

                        settings.setValue("playback/repeat", "no");
                        
                        if (ui->shuffle->isChecked() == Qt::Unchecked) {

                            settings.setValue("playback/shuffle", "no");

                        } else {

                            settings.setValue("playback/shuffle", "yes");

                        }

                    } else if (ui->shuffle->isChecked() == Qt::Unchecked) {

                        settings.setValue("playback/shuffle", "no");

                        if (ui->repeat->isChecked() == Qt::Unchecked) {

                            settings.setValue("playback/repeat", "no");

                        } else {

                            settings.setValue("playback/repeat", "yes");

                        }

                    } else {

                        settings.setValue("playback/repeat", "no");
                        settings.setValue("playback/shuffle", "no");

                    }

                }

                break;

            case QMediaPlaylist::CurrentItemInLoop :

                ui->repeat->setChecked(true);
                ui->shuffle->setChecked(false);
                settings.setValue("playback/shuffle", "no");
                settings.setValue("playback/repeat", "yes");

                break;

            case QMediaPlaylist::Sequential :

                ui->repeat->setChecked(false);
                ui->shuffle->setChecked(false);
                settings.setValue("playback/shuffle", "no");
                settings.setValue("playback/repeat", "no");

                break;

            case QMediaPlaylist::Loop :

                if (playListObj->mediaCount() > 1) {

                    ui->repeat->setChecked(false);
                    ui->shuffle->setChecked(false);
                    settings.setValue("playback/shuffle", "no");
                    settings.setValue("playback/repeat", "no");

                } else {

                    if (ui->repeat->isChecked() == Qt::Unchecked) {

                        settings.setValue("playback/repeat", "no");
                        
                        if (ui->shuffle->isChecked() == Qt::Unchecked) {

                            settings.setValue("playback/shuffle", "no");

                        } else {

                            settings.setValue("playback/shuffle", "yes");

                        }

                    } else if (ui->shuffle->isChecked() == Qt::Unchecked) {

                        settings.setValue("playback/shuffle", "no");

                        if (ui->repeat->isChecked() == Qt::Unchecked) {

                            settings.setValue("playback/repeat", "no");

                        } else {

                            settings.setValue("playback/repeat", "yes");

                        }

                    } else {

                        settings.setValue("playback/repeat", "no");
                        settings.setValue("playback/shuffle", "no");

                    }

                }

                break;

            case QMediaPlaylist::Random :

                if (playListObj->mediaCount() > 1) {

                    ui->repeat->setChecked(false);
                    ui->shuffle->setChecked(true);
                    settings.setValue("playback/shuffle", "yes");
                    settings.setValue("playback/repeat", "no");

                } else {

                    if (ui->repeat->isChecked() == Qt::Unchecked) {

                        settings.setValue("playback/repeat", "no");
                        
                        if (ui->shuffle->isChecked() == Qt::Unchecked) {

                            settings.setValue("playback/shuffle", "no");

                        } else {

                            settings.setValue("playback/shuffle", "yes");

                        }

                    } else if (ui->shuffle->isChecked() == Qt::Unchecked) {

                        settings.setValue("playback/shuffle", "no");

                        if (ui->repeat->isChecked() == Qt::Unchecked) {

                            settings.setValue("playback/repeat", "no");

                        } else {

                            settings.setValue("playback/repeat", "yes");

                        }

                    } else {

                        settings.setValue("playback/repeat", "no");
                        settings.setValue("playback/shuffle", "no");

                    }

                }

                break;

            default :

                break;

        }

    });

    connect(player, &QMediaPlayer::stateChanged, [this]() {

        const int mediaCount = playListObj->mediaCount();

        switch (player->state()) {

            case QMediaPlayer::PlayingState :

                setWindowTitle("BOSS Music Player - Playing");
                ui->tableView->viewport()->setAcceptDrops(false);
                playIconPlay();

                break;

            case QMediaPlayer::PausedState :

                setWindowTitle("BOSS Music Player");
                ui->tableView->viewport()->setAcceptDrops(true);
                playIconPause();

                break;

            case QMediaPlayer::StoppedState :

                setWindowTitle("BOSS Music Player");
                ui->tableView->viewport()->setAcceptDrops(true);

                if (!player->isMuted()) {

                    player->setVolume(0);

                }

                player->play();

                if (mediaCount > 1) {

                    if (ui->repeat->isChecked() != Qt::Unchecked) {

                        player->pause();

                        QModelIndex ind = playListDataModel->index(lastIndex, 0);
                        ui->tableView->selectionModel()->clearSelection();
                        ui->tableView->selectionModel()->setCurrentIndex(ind, QItemSelectionModel::Select);
                        playListObj->setCurrentIndex(ind.row());

                        player->play();

                    }

                }

                if (mediaCount == 1) {
                    
                    if (ui->repeat->isChecked() == Qt::Unchecked) {

                        player->pause();

                    }

                    ui->tableView->selectRow(0);
                    ui->currentSong->setText(trimFileExt(playListDataModel->data(playListDataModel->index(playListObj->currentIndex(), 0)).toString()));

                }

                if (!player->isMuted()) {

                    player->setVolume(ui->volumeBar->value());

                }

                break;

            default :

                break;

        }

    });

    if (QSystemTrayIcon::isSystemTrayAvailable()) {

        trayIcon = new QSystemTrayIcon();
        trayIcon->setIcon(appIco);

        connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));

        trayMenu = new QMenu();
        quitAction = new QAction(QIcon(QStringLiteral(TRAYQUITICO)), tr("Quit"));
        muteButtonAction = new QAction(QIcon(QStringLiteral(TRAYMUTEICO)), tr("Mute"));
        playButtonAction = new QAction(QIcon(QStringLiteral(TRAYPLAYICO)), tr("Play"));
        backButtonAction = new QAction(QIcon(QStringLiteral(TRAYRWICO)), tr("Back"));
        forwardButtonAction = new QAction(QIcon(QStringLiteral(TRAYFFICO)), tr("Forward"));

        connect(quitAction, SIGNAL(triggered()), this, SLOT(close()));
        connect(muteButtonAction, SIGNAL(triggered()), this, SLOT(on_mute_clicked()));
        connect(playButtonAction, SIGNAL(triggered()), this, SLOT(on_play_clicked()));
        connect(backButtonAction, SIGNAL(triggered()), this, SLOT(on_back_clicked()));
        connect(forwardButtonAction, SIGNAL(triggered()), this, SLOT(on_forward_clicked()));

        trayMenu->addAction(quitAction);
        trayMenu->addAction(muteButtonAction);
        trayMenu->addAction(playButtonAction);
        trayMenu->addAction(backButtonAction);
        trayMenu->addAction(forwardButtonAction);

        trayIcon->setContextMenu(trayMenu);

        if (settingTrayicon == "yes") {

            trayIcon->show();

        } else if (settingTrayicon == "no") {

            trayIcon->hide();

        }

    }

    ui->shuffle->installEventFilter(this);
    ui->repeat->installEventFilter(this);
    ui->play->installEventFilter(this);
    ui->back->installEventFilter(this);
    ui->forward->installEventFilter(this);
    ui->mute->installEventFilter(this);
    ui->save->installEventFilter(this);
    ui->empty->installEventFilter(this);
    ui->config->installEventFilter(this);
    ui->about->installEventFilter(this);
    ui->remove->installEventFilter(this);
    ui->add->installEventFilter(this);
    ui->searchBar->installEventFilter(this);
    ui->volumeBar->installEventFilter(this);
    ui->seekBar->installEventFilter(this);
    ui->tableView->viewport()->installEventFilter(this);

    ui->currentSong->setText(tr("Welcome!"));
    ui->seekBar->setDisabled(true);

    restoreLast();

/*

    For Windows 10, you can install xiph.org's directshow
    filters from https://xiph.org/dshow/downloads/ to
    enable ogg, weba, spx and flac file format playback

    https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types
    https://wiki.xiph.org/MIME_Types_and_File_Extensions
    https://doc.qt.io/qt-5/qmultimedia.html#SupportEstimate-enum

*/

    QString codecList;

    switch (QMediaPlayer::hasSupport("audio/wav")) {

        case QMultimedia::NotSupported :

            wav = false;

            break;

        default :

            codecList.append("*.wav");
            wav = true;

            break;

    }

    switch (QMediaPlayer::hasSupport("audio/flac")) {

        case QMultimedia::NotSupported :

            flac = false;

            break;

        default :

            codecList.append(" *.flac");
            flac = true;

            break;

    }

    switch (QMediaPlayer::hasSupport("audio/ogg")) {

        case QMultimedia::NotSupported :

            ogg = false;

            break;

        default :

            codecList.append(" *.ogg *.oga *.ogx *.opus *.spx");
            ogg = true;
            opus = true;
            spx = true;

            break;

    }

    switch (QMediaPlayer::hasSupport("audio/mpeg")) {

        case QMultimedia::NotSupported :

            mp3 = false;

            break;

        default :

            codecList.append(" *.mp3");
            mp3 = true;

            break;

    }

    if (!mp3) {

        switch (QMediaPlayer::hasSupport("audio/mp3")) {

            case QMultimedia::NotSupported :

                mp3 = false;

                break;

            default :

                codecList.append(" *.mp3");
                mp3 = true;

                break;

        }

    }

    switch (QMediaPlayer::hasSupport("audio/x-ms-wma")) {

        case QMultimedia::NotSupported :

            wma = false;

            break;

        default :

            codecList.append(" *.wma");
            wma = true;

            break;

    }

    if (!ogg) {

        switch (QMediaPlayer::hasSupport("audio/opus")) {

            case QMultimedia::NotSupported :

                opus = false;

                break;

            default :

                codecList.append(" *.opus");
                opus = true;

                break;

        }

    }

    switch (QMediaPlayer::hasSupport("audio/webm")) {

        case QMultimedia::NotSupported :

            weba = false;

            break;

        default :

            codecList.append(" *.weba");
            weba = true;

            break;

    }

    if (!weba) {

        switch (QMediaPlayer::hasSupport("audio/weba")) {

            case QMultimedia::NotSupported :

                weba = false;

                break;

            default :

                codecList.append(" *.weba");
                weba = true;

                break;

        }

    }

    if (!ogg) {

        switch (QMediaPlayer::hasSupport("audio/spx")) {

            case QMultimedia::NotSupported :

                spx = false;

                break;

            default :

                codecList.append(" *.spx");
                spx = true;

                break;

        }

    }

    switch (QMediaPlayer::hasSupport("audio/aac")) {

        case QMultimedia::NotSupported :

            aac = false;

            break;

        case QMultimedia::MaybeSupported :

            aac = false;

            break;

        default :

            codecList.append(" *.aac");
            aac = true;

            break;

    }

    switch (QMediaPlayer::hasSupport("audio/mp4")) {

        case QMultimedia::NotSupported :

            m4a = false;

            break;

        case QMultimedia::MaybeSupported :

            m4a = false;

            break;

        default :

            codecList.append(" *.m4a");
            m4a = true;

            break;

    }

    filters << "Music Files (" + codecList + ")";

    const QStringList args = QCoreApplication::arguments();
    const QString missingCodec = tr("Error: Sorry, please install the required codec for this file type");
    const QString unsupportedCodec = tr("Error: Sorry, no aac/m4a playback support on Windows 10");

    if (args.count() == 2) {

        playOption = args.at(1).toUtf8();

        if (playOption.endsWith(".m3u", Qt::CaseInsensitive)) {

            loadPlaylistFile(playOption);

        } else if (playOption.endsWith(".mp3", Qt::CaseInsensitive)) {

            if (mp3 == true) {

                singleFile(playOption);

            } else {

                ui->currentSong->setText(missingCodec);

            }

       } else if (playOption.endsWith(".wma", Qt::CaseInsensitive)) {

            if (wma == true) {

                singleFile(playOption);

            } else {

                ui->currentSong->setText(missingCodec);

            }

       } else if (playOption.endsWith(".ogg", Qt::CaseInsensitive)) {

            if (ogg == true) {

                singleFile(playOption);

            } else {

                ui->currentSong->setText(missingCodec);

            }

       } else if (playOption.endsWith(".wav", Qt::CaseInsensitive)) {

            if (wav == true) {

                singleFile(playOption);

            } else {

                ui->currentSong->setText(missingCodec);

            }

       } else if (playOption.endsWith(".oga", Qt::CaseInsensitive)) {

            if (ogg == true) {

                singleFile(playOption);

            } else {

                ui->currentSong->setText(missingCodec);

            }

       } else if (playOption.endsWith(".ogx", Qt::CaseInsensitive)) {

            if (ogg == true) {

                singleFile(playOption);

            } else {

                ui->currentSong->setText(missingCodec);

            }

       } else if (playOption.endsWith(".opus", Qt::CaseInsensitive)) {

            if (opus == true) {

                singleFile(playOption);

            } else {

                ui->currentSong->setText(missingCodec);

            }

       } else if (playOption.endsWith(".flac", Qt::CaseInsensitive)) {

            if (flac == true) {

                singleFile(playOption);

            } else {

                ui->currentSong->setText(missingCodec);

            }

       } else if (playOption.endsWith(".weba", Qt::CaseInsensitive)) {

            if (weba == true) {

                singleFile(playOption);

            } else {

                ui->currentSong->setText(missingCodec);

            }

       } else if (playOption.endsWith(".spx", Qt::CaseInsensitive)) {

            if (spx == true) {

                singleFile(playOption);

            } else {

                ui->currentSong->setText(missingCodec);

            }

        } else if (playOption.endsWith(".aac", Qt::CaseInsensitive)) {

            #ifndef Q_OS_WIN
                singleFile(playOption);
            #else
                ui->currentSong->setText(unsupportedCodec);
            #endif

        } else if (playOption.endsWith(".m4a", Qt::CaseInsensitive)) {

            #ifndef Q_OS_WIN
                singleFile(playOption);
            #else
                ui->currentSong->setText(unsupportedCodec);
            #endif

        }

    }

    if (playListObj->mediaCount() != 0) {

    } else {

        if (loadPlaylistsAtStart == "yes") {

            #ifdef Q_OS_WIN
                const QString pList = "BOSSPlaylist.m3u";
            #else
                const QString pList = QStandardPaths::standardLocations(QStandardPaths::ConfigLocation).at(0) + "BOSSPlaylist.m3u";
            #endif

            loadPlaylistFile(pList);

        }

    }

}

MainWindow::~MainWindow() {

    delete playListDataModel;
    delete ui;
    delete playListObj;
    delete player;

}

bool MainWindow::restoreLast() {

    QSettings settings("BOSS", "Player");

    QString muteLastState = settings.value("volume/mute", "no").toString().toLower();
    const int volumeLastState = settings.value("playback/volume", 100).toInt();

    if (volumeLastState == 0) {

        muteLastState = "yes";

    } else {

        player->setVolume(volumeLastState);
        ui->volumeBar->setValue(volumeLastState);

    }

    if (muteLastState == "yes") {

        on_mute_clicked();

    }

    const QString shuffleLastState = settings.value("playback/shuffle", "no").toString().toLower();
    const QString repeatLastState = settings.value("playback/repeat", "no").toString().toLower();

    if (playListObj->mediaCount() > 1) {

        if (shuffleLastState == "yes") {

            playListObj->setPlaybackMode(QMediaPlaylist::Random);

        } else if (repeatLastState == "yes") {

            playListObj->setPlaybackMode(QMediaPlaylist::CurrentItemOnce);

        } else {

            playListObj->setPlaybackMode(QMediaPlaylist::Loop);

        }

    } else {

        if (shuffleLastState == "yes") {

            ui->repeat->setChecked(false);
            ui->shuffle->setChecked(true);

        } else if (repeatLastState == "yes") {

            ui->repeat->setChecked(true);
            ui->shuffle->setChecked(false);

        }

        playListObj->setPlaybackMode(QMediaPlaylist::CurrentItemOnce);

    }

    return true;

}

bool MainWindow::themeStyles() {

    if (nativeTheme == "no") {
        appStyle.clear();
        // https://doc.qt.io/qt-5/stylesheet-reference.html | https://doc.qt.io/qt-5/stylesheet-examples.html
        appStyle.append(QString(
            "QWidget,QTableView::item:selected,QLineEdit,QMenu::separator,QLabel,QRadioButton::indicator:unchecked,QPushButton:hover,QPushButton:checked,QMenu,QMenu::item:selected,QRadioButton,QRadioButton:checked,QRadioButton:unchecked,QTableView::item{color:" + currentColour2.name() + "}"
            "QWidget{border-radius:.2em;font-family:Candara,Corbel,Calibri,Sans-serif;font-size:11px!important;margin:0;padding:0;text-shadow:" + currentColour2.name() + " 1px 0 10px}"
            "QTableView,QSlider::handle:horizontal:hover,QSlider::handle:vertical:hover,QPushButton:hover,QPushButton:checked,QScrollBar::handle:horizontal:hover,QScrollBar::handle:vertical:hover,QRadioButton::indicator:unchecked,QRadioButton::indicator:unchecked:pressed,QRadioButton::indicator:unchecked:hover,QRadioButton::indicator:checked{border-color:" + currentColour2.name() + "}"
            "QRadioButton::indicator:unchecked,QRadioButton::indicator:checked:pressed,QScrollBar::handle:horizontal,QScrollBar::handle:vertical,QScrollBar::handle:horizontal:hover,QScrollBar::handle:vertical:hover,QSlider::add-page:vertical:disabled,QSlider::sub-page:horizontal:disabled,QSlider::groove:horizontal,QSlider::groove:vertical,QSlider::add-page:vertical,QSlider::sub-page:horizontal{background-color:" + currentColour2.name() + "}"
            "QTableView::item:selected,QLineEdit,QPushButton,QPushButton:disabled,QSlider:disabled,QSpinBox,QRadioButton::indicator:unchecked:disabled,QSlider::groove:horizontal,QSlider::groove:vertical,QSlider::add-page:vertical,QSlider::sub-page:horizontal{border-color:" + currentColour3.name() + "}"
            "QTableView:selected,QPushButton:selected,QPushButton:disabled,QTableView:disabled,QRadioButton QWidget:disabled,QLabel::disabled{color:" + currentColour3.name() + "}"
            "QPushButton:disabled,QMenu::separator,QMainWindow,QColorDialog{background-color:" + currentColour3.name() + "}"
            "QTableView::item:selected:hover,QLineEdit:selected,QTableView::item:selected,QTableView:selected{color:" + currentColour4.name() + "}"
            "QPushButton:pressed:hover{border-color:" + currentColour1.lighter(120).name() + "}"
            "QTableView,QScrollBar:horizontal,QScrollBar:vertical,QPushButton:selected,QSlider::handle:horizontal:hover,QSlider::handle:vertical:hover,QSlider::handle:horizontal:hover,QSlider::handle:vertical:hover,QRadioButton::indicator:checked:disabled,QRadioButton::indicator:indeterminate,QRadioButton::indicator:indeterminate:disabled,QRadioButton::indicator:indeterminate:hover,QRadioButton::indicator:indeterminate:pressed,QPushButton:hover,QPushButton:checked,QRadioButton::indicator:unchecked:pressed,QRadioButton::indicator:unchecked:hover,QRadioButton::indicator:checked{background-color:" + currentColour1.lighter(120).name() + "}"
            "QScrollBar::handle:horizontal,QScrollBar::handle:vertical{border-color:" + currentColour1.name() + "}"
            "QTableView{alternate-background-color:" + currentColour1.name() + "}"
            "QTableView::item:hover,QTableView::item:selected:hover{background-color:" + currentColour1.darker(160).name() + "}"
            "QTableView::item:selected,QTableView:selected,QRadioButton::indicator:checked:hover,QTableView:disabled,QRadioButton QWidget:disabled,QLineEdit,QLineEdit:selected,QPushButton,QSlider::handle:horizontal,QSlider::handle:vertical,QMenu::item,QSpinBox{background-color:" + currentColour5.name() + "}"
            "QSlider::handle:horizontal,QSlider::handle:vertical{border-color:" + currentColour5.name() + "}"
            "QTableView::item,QScrollBar:horizontal,QScrollBar:vertical{border-color:transparent}"
            "QSlider:disabled,QScrollBar::up-arrow:horizontal,QScrollBar::down-arrow:horizontal,QScrollBar::up-arrow:vertical,QScrollBar::down-arrow:vertical,QScrollBar::add-page:horizontal,QScrollBar::sub-page:horizontal,QScrollBar::add-page:vertical,QScrollBar::sub-page:vertical,QRadioButton::indicator,QLabel,QLabel::disabled{background-color:transparent}"
            "QTableView,QTableView::item,QScrollBar:horizontal,QScrollBar:vertical,QScrollBar::handle:vertical,QScrollBar::handle:vertical:hover,QScrollBar::handle:horizontal,QScrollBar::handle:horizontal:hover,QLineEdit,QPushButton,QSlider::groove:vertical,QSlider::add-page:vertical{border-radius:.37em}"
            "QTableView,QTableView::item,QScrollBar:horizontal,QScrollBar:vertical,QScrollBar:horizontal,QScrollBar:vertical,QScrollBar::handle:vertical,QScrollBar::handle:vertical:hover,QScrollBar::handle:horizontal,QScrollBar::handle:horizontal:hover,QLineEdit,QPushButton,QSlider:disabled,QSlider::handle:horizontal,QSlider::handle:vertical,QSlider::handle:horizontal:hover,QSlider::handle:vertical:hover,QSlider::groove:horizontal,QSlider::sub-page:horizontal,QSlider::groove:vertical,QSlider::add-page:vertical,QSpinBox,QRadioButton::indicator:unchecked,QRadioButton::indicator:unchecked:hover,QRadioButton::indicator:unchecked:pressed,QRadioButton::indicator:checked,QRadioButton::indicator:unchecked:disabled{border-style:solid}"
            "QTableView,QTableView::item,QScrollBar:horizontal,QScrollBar:vertical,QScrollBar:horizontal,QScrollBar:vertical,QScrollBar::handle:vertical,QScrollBar::handle:vertical:hover,QScrollBar::handle:horizontal,QScrollBar::handle:horizontal:hover,QLineEdit,QPushButton,QSlider::handle:horizontal,QSlider::handle:vertical,QSlider::handle:horizontal:hover,QSlider::handle:vertical:hover,QSlider::groove:horizontal,QSlider::sub-page:horizontal,QSlider::groove:vertical,QSlider::add-page:vertical,QSpinBox,QRadioButton::indicator:unchecked,QRadioButton::indicator:unchecked:hover,QRadioButton::indicator:unchecked:pressed,QRadioButton::indicator:checked,QRadioButton::indicator:unchecked:disabled{border-width:.1em}"
            "QTableView,QTableView::item,QScrollBar::add-line:horizontal,QPushButton,QSlider::groove:horizontal,QSlider::sub-page:horizontal,QSlider::groove:vertical,QSlider::add-page:vertical,QMenu::icon,QLabel{margin:0}"
            "QTableView,QTableView::item,QScrollBar:horizontal,QScrollBar:vertical,QPushButton{outline-style:none}"
            "QTableView,QTableView::item,QPushButton{padding:.2em}"
            "QScrollBar:horizontal,QScrollBar:vertical{border-radius:.27em}"
            "QScrollBar:horizontal{height:1em;margin:.15em 1em}"
            "QScrollBar:vertical{margin:1em .15em;width:1em}"
            "QScrollBar::handle:vertical,QScrollBar::handle:vertical:hover{min-height:.53em}"
            "QScrollBar::add-line:horizontal,QScrollBar::sub-line:horizontal,QSpinBox::up-button,QSpinBox::down-button,QScrollBar::add-line:vertical,QScrollBar::sub-line:vertical,QLabel,QLabel::disabled{border-style:none}"
            "QScrollBar::add-line:horizontal,QScrollBar::sub-line:horizontal,QScrollBar::add-line:horizontal:hover,QScrollBar::add-line:horizontal:on,QScrollBar::add-line:vertical:hover,QScrollBar::add-line:vertical:on,QScrollBar::sub-line:horizontal:hover,QScrollBar::sub-line:horizontal:on,QScrollBar::sub-line:vertical:hover,QScrollBar::sub-line:vertical:on,QScrollBar::add-line:vertical,QScrollBar::sub-line:vertical{height:.66em}"
            "QScrollBar::add-line:horizontal,QScrollBar::sub-line:horizontal,QScrollBar::add-line:vertical,QScrollBar::sub-line:vertical,QScrollBar::add-line:horizontal:hover,QScrollBar::add-line:horizontal:on,QScrollBar::add-line:vertical:hover,QScrollBar::add-line:vertical:on,QScrollBar::sub-line:horizontal:hover,QScrollBar::sub-line:horizontal:on,QScrollBar::sub-line:vertical:hover,QScrollBar::sub-line:vertical:on{width:.66em}"
            "QScrollBar::sub-line:horizontal{margin:0 .2em}"
            "QScrollBar::handle:horizontal,QScrollBar::handle:horizontal:hover{min-width:.53em}"
            "QLineEdit{padding:.15em .37em}"
            "QPushButton,QLabel{font-weight:700}"
            "QPushButton{max-height:2.9em;max-width:8em;min-height:.9em;min-width:2.7em;overflow:hidden;text-overflow:ellipsis;word-break:break-word}"
            "QSlider:disabled{border-radius:.3em;border-width:.15em}"
            "QSlider::handle:horizontal,QSlider::handle:vertical,QSlider::groove:horizontal,QSlider::sub-page:horizontal{border-radius:.17em}"
            "QSlider::handle:horizontal,QSlider::handle:vertical{height:.53em}"
            "QSlider::handle:horizontal,QSlider::handle:vertical{width:.53em}"
            "QSlider::handle:horizontal{margin:-.53em 0}"
            "QSlider::handle:vertical{margin:0 -.53em}"
            "QSlider::groove:horizontal,QSlider::sub-page:horizontal{height:.37em}"
            "QSlider::groove:vertical,QSlider::add-page:vertical{width:.37em}"
            "QMenu{padding:.37em}"
            "QMenu::icon{padding-left:.37em}"
            "QMenu::indicator{height:1.15em;padding-left:.4em;width:1.15em}"
            "QLabel,QRadioButton{padding:.15em}"
            "QSpinBox{max-height:1.55em;min-height:1.55em;min-width:3.53em}"
            "QSpinBox::up-arrow,QSpinBox::down-arrow{background-position:center;background-repeat:no-repeat}"
            "QRadioButton{margin-left:.2em;margin-right:.2em}"
            "QRadioButton::indicator:unchecked,QRadioButton::indicator:unchecked:hover,QRadioButton::indicator:unchecked:pressed,QRadioButton::indicator:checked{border-radius:.4em}"
            "QScrollBar::add-line:vertical,QScrollBar::sub-line:vertical{margin:.2em 0}"
            "QSpinBox::up-arrow{background-image:url(:/resources/chevron-up-grey.svg)}"
            "QSpinBox::down-arrow{background-image:url(:/resources/chevron-down-grey.svg)}"
            "QSpinBox::up-arrow:hover{background-image:url(:/resources/chevron-up-white.svg)}"
            "QSpinBox::down-arrow:hover{background-image:url(:/resources/chevron-down-white.svg)}"
        ));

        setStyleSheet(appStyle);

    }    

    return true;

}

bool MainWindow::singleFile(QString line) {

    ui->play->setIcon(QIcon(PAUSEICO));
    ui->save->setChecked(true);
    playListObj->clear();
    bool newItem = false;

    if (line.lastIndexOf("http", -1, Qt::CaseInsensitive) == 0) {

        QList < QStandardItem* > items;
        QStandardItem *item1 = new QStandardItem(QUrl(line).fileName());
        item1->setDragEnabled(true);
        item1->setDropEnabled(false);
        item1->setTextAlignment(Qt::AlignCenter);
        items.append(item1);
        playListDataModel->insertRow(0, items);
        playListDataModel->setRowCount(1);
        playListObj->addMedia(QUrl(line));
        newItem = true;

    } else {

        line.replace("file:///", "");
        QString filePath = QDir(line).absolutePath();

        if (fileExists(filePath)) {

            QList < QStandardItem* > items;
            QStandardItem *item1 = new QStandardItem(QDir(line).dirName());
            item1->setDragEnabled(true);
            item1->setDropEnabled(false);
            item1->setTextAlignment(Qt::AlignCenter);
            items.append(item1);
            playListDataModel->insertRow(0, items);
            playListDataModel->setRowCount(1);
            playListObj->addMedia(QUrl::fromLocalFile(filePath));
            newItem = true;

        }

    }

    playListObj->setPlaybackMode(QMediaPlaylist::CurrentItemOnce);

    if (newItem == true) {

        switch (player->state()) {

            case QMediaPlayer::PausedState :

                player->play();
                playIconPlay();

                break;

            case QMediaPlayer::StoppedState :

                player->play();
                playIconPlay();

                break;

            default :

                break;

        }

        ui->seekBar->setEnabled(true);

        return true;

    } else {

        return false;

    }

}

QString MainWindow::trimFileExt(QString fileName) {

    return fileName.left(fileName.lastIndexOf("."));

}

bool MainWindow::playIconPause() {

    ui->play->setChecked(false);
    ui->play->setIcon(QIcon(PLAYICO));

    if (QSystemTrayIcon::isSystemTrayAvailable()) {

        playButtonAction->setIcon(QIcon(TRAYPLAYICO));
        playButtonAction->setText(tr("Play"));

    }

    return true;

}

bool MainWindow::playIconPlay() {

    ui->play->setChecked(true);
    ui->play->setIcon(QIcon(PAUSEICO));
    int index = playListObj->currentIndex();
    messagePreTrim = playListDataModel->data(playListDataModel->index(index, 0)).toString();
    messageTextNew = trimFileExt(messagePreTrim);
    ui->currentSong->setText(messageTextNew);
    messagePreTrim = "";
    messageTextNew = "";
    QModelIndex ind = playListDataModel->index(index, 0);
    ui->tableView->selectionModel()->clearSelection();
    ui->tableView->selectionModel()->setCurrentIndex(ind, QItemSelectionModel::Select);

    if (QSystemTrayIcon::isSystemTrayAvailable()) {

        playButtonAction->setIcon(QIcon(TRAYPAUSEICO));
        playButtonAction->setText(tr("Pause"));

    }

    return true;

}

bool MainWindow::fileExists(QString path) {

    QFileInfo check_file(path);

    return check_file.exists() && check_file.isFile();

}

bool MainWindow::playNow() {

    ui->seekBar->setEnabled(true);

    switch (player->state()) {

        case QMediaPlayer::PlayingState :

            break;

        case QMediaPlayer::PausedState :

            if (settingAutoplay == "yes") {

                player->play();

            } else {

                player->play();
                player->pause();

            }

            break;

        case QMediaPlayer::StoppedState :

            if (settingAutoplay == "yes") {

                player->play();
                playIconPlay();

            } else {

                player->play();
                player->pause();

            }

            break;

        default :

            break;

    }

    return true;

}

bool MainWindow::loadPlaylistFile(QString m3uPlaylistFile) {

    if (fileExists(m3uPlaylistFile)) {

        QFile file(m3uPlaylistFile);

        if (!file.open(QIODevice::ReadOnly|QIODevice::Text)) {

            ui->currentSong->setText(tr("Error: Unable to load playlist")); //QMessageBox::information(0, "error", file.errorString());
            return false;

        } else {

            QTextStream in(&file);
            in.setCodec("UTF-8");
            QStringList duplicateCheck;

            while (!in.atEnd()) {

                QString pathToCheck = in.readLine().toUtf8();

                if (!pathToCheck.isEmpty()) {

                    duplicateCheck.append(pathToCheck);

                }

            }

            duplicateCheck.removeDuplicates();
            int lPlayed = 0;
            bool hasLastPlayed = false;

            for (int i = 0; i < duplicateCheck.count(); ++i) {

                QString line = duplicateCheck.at(i);

                if (line.lastIndexOf("http", -1, Qt::CaseInsensitive) == 0) {

                    QList < QStandardItem* > items;
                    QStandardItem *item1 = new QStandardItem(QUrl(line).fileName());
                    item1->setDragEnabled(true);
                    item1->setDropEnabled(false);
                    item1->setTextAlignment(Qt::AlignCenter);
                    items.append(item1);
                    playListDataModel->appendRow(items);
                    playListObj->addMedia(QUrl(line));

                    if (!lastPlayed.isEmpty()) {

                        if (line.contains(lastPlayed)) {

                            lPlayed = i;
                            hasLastPlayed = true;

                        }

                    }

                } else {

                    line.replace("file:///", "");
                    QString filePath = QDir(line).absolutePath();

                    if (fileExists(filePath)) {

                        QList < QStandardItem* > items;
                        QStandardItem *item1 = new QStandardItem(QDir(line).dirName());
                        item1->setDragEnabled(true);
                        item1->setDropEnabled(false);
                        item1->setTextAlignment(Qt::AlignCenter);
                        items.append(item1);
                        playListDataModel->appendRow(items);
                        playListObj->addMedia(QUrl::fromLocalFile(filePath));

                        if (!lastPlayed.isEmpty()) {

                            if (line.contains(lastPlayed)) {

                                lPlayed = i;
                                hasLastPlayed = true;

                            }

                        }

                    }

                }

            }

            playListDataModel->setRowCount(duplicateCheck.count());

            file.close();
            ui->save->setChecked(false);
            loadSettings();

            if (playListObj->mediaCount() > 1) {

                if (ui->repeat->isChecked() == Qt::Unchecked) {

                    if (ui->shuffle->isChecked() == Qt::Unchecked) {

                        playListObj->setPlaybackMode(QMediaPlaylist::Loop);

                    } else {

                        playListObj->setPlaybackMode(QMediaPlaylist::Random);

                    }

                }

            } else {

                playListObj->setPlaybackMode(QMediaPlaylist::CurrentItemOnce);

            }

            if (playListObj->mediaCount() > 0) {

                if (loadPlaylistsAtStart == "yes") {

                    if (resumePlayback == "yes") {

                        if (!lastPlayed.isEmpty()) {

                            if (hasLastPlayed == true) {

                                QModelIndex in = playListDataModel->index(lPlayed, 0);  
                                ui->tableView->selectionModel()->clearSelection();
                                ui->tableView->selectionModel()->setCurrentIndex(in, QItemSelectionModel::Select);
                                playListObj->setCurrentIndex(lPlayed);
                                ui->tableView->selectRow(lPlayed);

                            }

                        }

                    }

                }

                playNow();
                currentPlaylistFile = m3uPlaylistFile;

                return true;

            } else {

                return false;


            }

        }

    } else {

        return false;

    }

}

bool MainWindow::writePlaylistFile(QString currentPlaylistFile) {

    QString m3uFile;

    #ifdef Q_OS_WIN

        if (currentPlaylistFile == "none.m3u") {

            m3uFile = "BOSSPlaylist.m3u";

        }

    #else

    if (currentPlaylistFile == "none.m3u") {

        m3uFile = QStandardPaths::standardLocations(QStandardPaths::ConfigLocation).at(0) + "BOSSPlaylist.m3u";

    }

    #endif

    if (currentPlaylistFile != "none.m3u") {

        m3uFile = currentPlaylistFile;
    }

    QFile file(m3uFile);

    if (!file.open(QIODevice::WriteOnly|QIODevice::Text)) {

        ui->currentSong->setText(tr("Error: Unable to open playlist to write to it")); //QMessageBox::information(0,"error",file.errorString());

        return false;

    } else {

        file.close();
        file.remove();

        if (!playListObj->save(QUrl::fromLocalFile(m3uFile), "m3u")) {

            ui->currentSong->setText(tr("Error: Unable to write playlist"));

        }

        return true;

    }

}

bool MainWindow::dataModelUpdate(QStandardItem *item) {

    const QString itemShortName = item->text();
    const int itemRowNumber = item->row();
    const int itemCount = playListObj->mediaCount();
    const int curIdx = playListObj->currentIndex();
    const QString idxItem = playListObj->media(curIdx).request().url().toLocalFile();

    for (int b = 0; b < itemCount; ++b) {

        const QString item = playListObj->media(b).request().url().toLocalFile();

        if (item.contains(itemShortName)) {

            if (b != itemRowNumber) {

                playListObj->moveMedia(b, itemRowNumber);

                if (item == idxItem) {

                }

            }

        }

    }

    return true;

}

bool MainWindow::seek(int seconds) {

    player->setPosition(double(seconds) * 1000);

    return true;

}

bool MainWindow::durationChanged(qint64 duration) {

    maxDuration = duration / 1000;
    ui->seekBar->setMaximum(maxDuration);

    return true;

}

bool MainWindow::positionChanged(qint64 progress) {

    if (!ui->seekBar->isSliderDown()) {

        ui->seekBar->setValue(progress / 1000);
        
    }

    return true;

}

bool MainWindow::on_seekBar_sliderPressed() {

    if (!player->isMuted()) {

        player->setVolume(0);

    }

    return true;

}

bool MainWindow::on_seekBar_sliderReleased() {

    if (!player->isMuted()) {

        player->setVolume(ui->volumeBar->value());

    }

    return true;

}

void MainWindow::closeEvent(QCloseEvent *event) {

    if (config) {

        configClose_released(true);

    }

    QSettings settings("BOSS", "Player");
    (player->isMuted()) ? settings.setValue("volume/mute", "yes") : settings.setValue("volume/mute", "no");
    (ui->repeat->isChecked() == Qt::Unchecked) ? settings.setValue("playback/repeat", "no") : settings.setValue("playback/repeat", "yes");
    (ui->shuffle->isChecked() == Qt::Unchecked) ? settings.setValue("playback/shuffle", "no") : settings.setValue("playback/shuffle", "yes");

    settings.setValue("playback/volume", ui->volumeBar->value());

    const QString cT = playListObj->media(playListObj->currentIndex()).request().url().toString();
    const QFile fileA(QUrl(playListObj->media(playListObj->currentIndex()).request().url()).fileName());
    const QFileInfo fileInfo(fileA);
    const QString cTrackName = fileInfo.fileName();
    settings.setValue("playback/lasttrack", cTrackName);

    if (QSystemTrayIcon::isSystemTrayAvailable()) {

        if (settingTrayicon == "yes") {

            trayIcon->hide();

        }

        delete quitAction;
        delete muteButtonAction;
        delete playButtonAction;
        delete backButtonAction;
        delete forwardButtonAction;
        delete trayMenu;
        delete trayIcon;

    }

    switch (player->state()) {

        case QMediaPlayer::PlayingState :

            player->stop();

            break;

        default :

            break;

    }

    return QMainWindow::closeEvent(event); // forward the event

}

bool MainWindow::iconActivated(QSystemTrayIcon::ActivationReason reason) {

    switch (reason) {

        case QSystemTrayIcon::Trigger :

            if (!this->isVisible()) {

                this->show();
                this->setWindowState( (windowState() & ~Qt::WindowMinimized) | Qt::WindowActive);
                this->raise();
                this->activateWindow();

            } else {

                this->hide();

            }

            break;

        default :

            break;

    }

    return true;

}

bool MainWindow::on_add_clicked() {

    ui->add->setChecked(false);
    QFileDialog dialog(this);
    dialog.setNameFilters(filters);
    dialog.setViewMode(QFileDialog::List);
    dialog.setFileMode(QFileDialog::ExistingFiles);

    #ifndef Q_OS_ANDROID

        dialog.setDirectory(QStandardPaths::standardLocations(QStandardPaths::MusicLocation).at(0));

    #endif

    if (dialog.exec()) {

        files = dialog.selectedFiles();

    }

    if (!files.empty()) {

        if (files.count() <= 10000) {

            ui->play->setIcon(QIcon(PAUSEICO));
            ui->save->setChecked(true);

            QStringList duplicateCheck;

            foreach (const QString &pathToCheck, files) {

                if (!pathToCheck.isEmpty()) {

                    duplicateCheck.append(pathToCheck.toUtf8());

                }

            }

            duplicateCheck.removeDuplicates();

            QStringList itemList1;
            const int itemCount1 = playListObj->mediaCount();

            if (itemCount1 >= 1) {

                for (int u = 0; u < itemCount1; ++u) {

                    const QString item = playListObj->media(u).request().url().toLocalFile();

                    if (!item.isEmpty()) {

                        itemList1.append(item);

                    }

                }
            }

#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)
            QSet<QString> filteredItemList = duplicateCheck.toSet().subtract(itemList1.toSet()); // Old way
#else
            QSet<QString> filteredItemList = QSet<QString>(duplicateCheck.begin(),duplicateCheck.end()).subtract(QSet<QString>(itemList1.begin(),itemList1.end())); // New way
#endif

            QStringList itemList2;

            foreach (const QString &fileName, filteredItemList) {

                itemList2.append(fileName);

            }

            for (int i = 0; i < itemList2.count(); ++i) {

                QString line = itemList2.at(i);

                if (line.lastIndexOf("http", -1, Qt::CaseInsensitive) == 0) {

                    QList < QStandardItem* > items;
                    QStandardItem *item1 = new QStandardItem(QUrl(line).fileName());
                    item1->setDragEnabled(true);
                    item1->setDropEnabled(false);
                    item1->setTextAlignment(Qt::AlignCenter);
                    items.append(item1);
                    playListDataModel->appendRow(items);
                    playListObj->addMedia(QUrl(line));

                } else {

                    line.replace("file:///", "");
                    QString filePath = QDir(line).absolutePath();

                    if (fileExists(filePath)) {

                        QList < QStandardItem* > items;
                        QStandardItem *item1 = new QStandardItem(QDir(line).dirName());
                        item1->setDragEnabled(true);
                        item1->setDropEnabled(false);
                        item1->setTextAlignment(Qt::AlignCenter);
                        items.append(item1);
                        playListDataModel->appendRow(items);
                        playListObj->addMedia(QUrl::fromLocalFile(filePath));

                    }

                }

            }

            playListDataModel->setRowCount(itemList1.count() + itemList2.count());

            loadSettings();

            if (playListDataModel->rowCount() > 1) {


                if (ui->repeat->isChecked() == Qt::Unchecked) {

                    if (ui->shuffle->isChecked() == Qt::Unchecked) {

                        playListObj->setPlaybackMode(QMediaPlaylist::Loop);

                    } else {

                        playListObj->setPlaybackMode(QMediaPlaylist::Random);

                    }

                }

            } else if (playListDataModel->rowCount() == 1) {

                playListObj->setPlaybackMode(QMediaPlaylist::CurrentItemOnce);

            }

            if (playListDataModel->rowCount() >= 1) {

                ui->play->setIcon(QIcon(PAUSEICO));
                ui->save->setChecked(true);
                playNow();

            } else {

                ui->play->setIcon(QIcon(PLAYICO));
                ui->save->setChecked(false);

            }

            files.clear();

        }

    }

    return true;

}

bool MainWindow::on_remove_clicked() {

    ui->remove->setChecked(false);

    if (playListObj->mediaCount() > 0) {

        int index = getIndex();

        if (index != -1) {

            ui->save->setChecked(true);
            playListObj->removeMedia(index);
            playListDataModel->removeRows(index, 1);

        }

    }

    return true;

}

bool MainWindow::on_save_clicked() {

    ui->save->setChecked(false);
    writePlaylistFile(currentPlaylistFile);

    return true;

}

bool MainWindow::on_empty_clicked() {

    ui->empty->setChecked(false);

    if (playListObj->mediaCount() > 0) {

        playListDataModel->removeRows(0, playListDataModel->rowCount());
        playListDataModel->setRowCount(0);
        playListObj->clear();
        ui->currentSong->setText(tr("Add some files to the playlist to start"));
        ui->save->setChecked(true);
        ui->seekBar->setDisabled(true);

        playIconPause();

    }

    return true;

}

bool MainWindow::getColour1() {

    newColour1 = QColorDialog::getColor(currentColour1, this, tr("Choose a colour"));

    if (newColour1.isValid()) {

        QSettings settings("BOSS", "Player");
        settings.setValue("theme/maincolour", newColour1);
        currentColour1 = newColour1;
        setStyleSheet(QString(""));
        themeStyles();

    }

    return true;

}

bool MainWindow::getColour2() {

    newColour2 = QColorDialog::getColor(currentColour2, this, tr("Choose a colour"));

    if (newColour2.isValid()) {

        QSettings settings("BOSS", "Player");
        settings.setValue("theme/textprimary", newColour2);
        currentColour2 = newColour2;
        setStyleSheet(QString(""));
        themeStyles();

    }

    return true;

}

bool MainWindow::getColour3() {

    newColour3 = QColorDialog::getColor(currentColour3, this, tr("Choose a colour"));

    if (newColour3.isValid()) {

        QSettings settings("BOSS", "Player");
        settings.setValue("theme/background", newColour3);
        currentColour3 = newColour3;
        setStyleSheet(QString(""));
        themeStyles();

    }

    return true;

}

bool MainWindow::getColour4() {

    newColour4 = QColorDialog::getColor(currentColour4, this, tr("Choose a colour"));

    if (newColour4.isValid()) {

        QSettings settings("BOSS", "Player");
        settings.setValue("theme/textsecondary", newColour4);
        currentColour4 = newColour4;
        setStyleSheet(QString(""));
        themeStyles();

    }

    return true;

}

bool MainWindow::getColour5() {

    newColour5 = QColorDialog::getColor(currentColour5, this, tr("Choose a colour"));

    if (newColour5.isValid()) {

        QSettings settings("BOSS", "Player");
        settings.setValue("theme/buttoncolour", newColour5);
        currentColour5 = newColour5;
        setStyleSheet(QString(""));
        themeStyles();

    }

    return true;

}

bool MainWindow::colourButtons() {

    QSettings settings("BOSS", "Player");

    if (radio3->isChecked() == Qt::Unchecked) {

        settings.setValue("theme/native", "no");
        colorButton1->show();
        colorButton2->show();
        colorButton3->show();
        colorButton4->show();
        colorButton5->show();
        colorButton6->show();
        nativeTheme = "no";
        themeStyles();

    } else {

        settings.setValue("theme/native", "yes");
        nativeTheme = "yes";
        setStyleSheet(QString(""));
        colorButton1->hide();
        colorButton2->hide();
        colorButton3->hide();
        colorButton4->hide();
        colorButton5->hide();
        colorButton6->hide();

    }

    return true;

}

bool MainWindow::themeColourReset() {

    currentColour1 = QColor(0, 85, 0, 255);
    currentColour2 = QColor(204, 204, 204, 255);
    currentColour3 = QColor(90, 90, 92, 255);
    currentColour4 = QColor(255, 255, 255, 255);
    currentColour5 = QColor(32, 32, 32, 255);

    QSettings settings("BOSS", "Player");

    settings.setValue("theme/maincolour", currentColour1);
    settings.setValue("theme/textprimary", currentColour2);
    settings.setValue("theme/background", currentColour3);
    settings.setValue("theme/textsecondary", currentColour4);
    settings.setValue("theme/buttoncolour", currentColour5);

    setStyleSheet(QString(""));
    themeStyles();

    return true;

}

bool MainWindow::on_config_clicked() {

    config = !config;

    if (config) {

        ui->tableView->hide();
        ui->searchBar->hide();

        loadSettings();

        configLabel = new QLabel(tr("Settings"));
        ui->verticalLayout->addWidget(configLabel);

        colorButton1 = new QPushButton(tr("&Main Colour"), this);
        connect(colorButton1, &QPushButton::clicked, this, &MainWindow::getColour1);
        ui->verticalLayout->addWidget(colorButton1);

        colorButton2 = new QPushButton(tr("&Text Colour"), this);
        connect(colorButton2, &QPushButton::clicked, this, &MainWindow::getColour2);
        ui->verticalLayout->addWidget(colorButton2);

        colorButton3 = new QPushButton(tr("&Fill Colour"), this);
        connect(colorButton3, &QPushButton::clicked, this, &MainWindow::getColour3);
        ui->verticalLayout->addWidget(colorButton3);

        colorButton4 = new QPushButton(tr("&Highlight Colour"), this);
        connect(colorButton4, &QPushButton::clicked, this, &MainWindow::getColour4);
        ui->verticalLayout->addWidget(colorButton4);

        colorButton5 = new QPushButton(tr("Button &Colour"), this);
        connect(colorButton5, &QPushButton::clicked, this, &MainWindow::getColour5);
        ui->verticalLayout->addWidget(colorButton5);

        colorButton6 = new QPushButton(tr("&Reset Colours"), this);
        connect(colorButton6, &QPushButton::clicked, this, &MainWindow::themeColourReset);
        ui->verticalLayout->addWidget(colorButton6);

        radio3 = new QRadioButton(tr("&Use native OS theme"));
        radio3->setAutoExclusive(false);
        connect(radio3, &QRadioButton::clicked, this, &MainWindow::colourButtons);

        if (nativeTheme == "yes") {

            radio3->setChecked(true);
            colorButton1->hide();
            colorButton2->hide();
            colorButton3->hide();
            colorButton4->hide();
            colorButton5->hide();
            colorButton6->hide();

        } else if (nativeTheme == "no") {

            radio3->setChecked(false);

        }

        ui->verticalLayout->addWidget(radio3);

        if (QSystemTrayIcon::isSystemTrayAvailable()) {

            radio1 = new QRadioButton(tr("&Enable the application tray icon"));
            radio1->setAutoExclusive(false);

            if (settingTrayicon == "yes") {

                radio1->setChecked(true);

            } else if (settingTrayicon == "no") {

                radio1->setChecked(false);

            }

            ui->verticalLayout->addWidget(radio1);

        }

        radio5 = new QRadioButton(tr("&Load the default playlist at start up"));
        radio5->setAutoExclusive(false);

        if (loadPlaylistsAtStart == "yes") {

            radio5->setChecked(true);

        } else if (loadPlaylistsAtStart == "no") {

            radio5->setChecked(false);

        }

        ui->verticalLayout->addWidget(radio5);

        radio6 = new QRadioButton(tr("&Try to resume playback at start up"));
        radio6->setAutoExclusive(false);

        if (resumePlayback == "yes") {

            radio6->setChecked(true);

        } else if (resumePlayback == "no") {

            radio6->setChecked(false);

        }

        ui->verticalLayout->addWidget(radio6);

        radio2 = new QRadioButton(tr("&Playlists will play automatically"));
        radio2->setAutoExclusive(false);

        if (settingAutoplay == "yes") {

            radio2->setChecked(true);

        } else if (settingAutoplay == "no") {

            radio2->setChecked(false);

        }

        ui->verticalLayout->addWidget(radio2);

        radio4 = new QRadioButton(tr("&Enable ESC key to exit"));
        radio4->setAutoExclusive(false);

        if (escapeKeyExit == "yes") {

            radio4->setChecked(true);

        } else if (escapeKeyExit == "no") {

            radio4->setChecked(false);

        }

        ui->verticalLayout->addWidget(radio4);

        ui->config->setChecked(true);
        config = true;

    } else {

        config = true;
        configClose_released(false);

    }

    return true;

}

bool MainWindow::configClose_released(bool exiting) {

    if (config) {

        QSettings settings("BOSS", "Player");

        if (QSystemTrayIcon::isSystemTrayAvailable()) {

            if (radio1->isChecked() == Qt::Unchecked) {

                settings.setValue("application/trayicon", "no");
                trayIcon->hide();

            } else {

                settings.setValue("application/trayicon", "yes");
                trayIcon->show();

            }

        }

        if (radio2->isChecked() == Qt::Unchecked) {

            settings.setValue("playlist/autoplay", "no");

        } else {

            settings.setValue("playlist/autoplay", "yes");

        }

        if (radio3->isChecked() == Qt::Unchecked) {

            settings.setValue("theme/native", "no");

        } else {

            settings.setValue("theme/native", "yes");

        }

        if (radio4->isChecked() == Qt::Unchecked) {

            settings.setValue("application/esckeytoexit", "no");

        } else {

            settings.setValue("application/esckeytoexit", "yes");

        }

        if (radio5->isChecked() == Qt::Unchecked) {

            settings.setValue("application/loadplaylistsatstart", "no");

        } else {

            settings.setValue("application/loadplaylistsatstart", "yes");

        }

        if (radio6->isChecked() == Qt::Unchecked) {

            settings.setValue("application/resumeplayback", "no");

        } else {

            settings.setValue("application/resumeplayback", "yes");

        }

        if (!exiting) {

            delete configLabel;

            if (QSystemTrayIcon::isSystemTrayAvailable()) {

                delete radio1;

            }

            delete radio2;
            delete radio4;
            delete radio5;
            delete radio6;
            delete radio3;
            delete colorButton1;
            delete colorButton2;
            delete colorButton3;
            delete colorButton4;
            delete colorButton5;
            delete colorButton6;

            ui->tableView->show();
            ui->searchBar->show();

        }

        ui->config->setChecked(false);

    }

    config = false;
    
    ui->config->setChecked(false);

    return true;

}

bool MainWindow::loadSettings() {

    QSettings settings("BOSS", "Player");

    if (QSystemTrayIcon::isSystemTrayAvailable()) {

        settingTrayicon = settings.value("application/trayicon", "yes").toString().toLower();

    }

    settingAutoplay = settings.value("playlist/autoplay", "yes").toString().toLower();
    nativeTheme = settings.value("theme/native", "no").toString().toLower();
    escapeKeyExit = settings.value("application/esckeytoexit", "yes").toString().toLower();
    loadPlaylistsAtStart = settings.value("application/loadplaylistsatstart", "yes").toString().toLower();
    resumePlayback = settings.value("application/resumeplayback", "yes").toString().toLower();
    lastPlayed = settings.value("playback/lasttrack", "").toString();

    currentColour1 = settings.value("theme/maincolour").value<QColor>();

    if (!currentColour1.isValid()) {

        currentColour1 = QColor(0, 85, 0, 255);

    }

    currentColour2 = settings.value("theme/textprimary").value<QColor>();

    if (!currentColour2.isValid()) {

        currentColour2 = QColor(204, 204, 204, 255);

    }

    currentColour3 = settings.value("theme/background").value<QColor>();

    if (!currentColour3.isValid()) {

        currentColour3 = QColor(90, 90, 92, 255);

    }

    currentColour4 = settings.value("theme/textsecondary").value<QColor>();

    if (!currentColour4.isValid()) {

        currentColour4 = QColor(255, 255, 255, 255);

    }

    currentColour5 = settings.value("theme/buttoncolour").value<QColor>();

    if (!currentColour5.isValid()) {

        currentColour5 = QColor(32, 32, 32, 255);

    }

    return true;

}

bool MainWindow::on_about_clicked() {

    const QString link = "https://smoothswim.gitlab.io/boss/";
    ui->about->setChecked(false);
    QDesktopServices::openUrl(QUrl(link));
/*  // Playlist & datamodel diagnostic dumps
    dumpModelData();
    dumpPlaylistData();
*/
    return true;

}

bool MainWindow::on_tableView_doubleClicked(const QModelIndex &index) {

    if (playListObj->mediaCount() > 0) {

        playIconPlay();

        ui->searchBar->clear();

        switch (player->state()) {

            case QMediaPlayer::PlayingState :

                break;

            case QMediaPlayer::PausedState :

                player->play();
                playIconPlay();

                break;

            case QMediaPlayer::StoppedState :

                player->play();
                playIconPlay();

                break;

            default :

                break;

        }

    }

    return true;

}

bool MainWindow::on_play_clicked() {

    if (playListObj->mediaCount() > 0) {

        switch (player->state()) {

            case QMediaPlayer::PlayingState :

                player->pause();
                playIconPause();
                ui->seekBar->setEnabled(true);

                break;

            case QMediaPlayer::PausedState :

                player->play();
                playIconPlay();
                ui->seekBar->setEnabled(true);

                break;

            case QMediaPlayer::StoppedState :

                // Not implemented. There isn't a stop button attached to the UI

                break;

            default :

                break;

        }

    } else {

        player->pause();
        player->setPosition(0);
        ui->tableView->selectRow(0);
        ui->currentSong->setText(tr("Add some files to the playlist to begin playback"));

        playIconPause();

    }

    return true;

}

bool MainWindow::on_forward_clicked() {

    if (playListObj->mediaCount() > 1) {

        forward();

    }

    return true;

}

bool MainWindow::on_back_clicked() {

    if (playListObj->mediaCount() > 1) {

        back();

    } else if (playListObj->mediaCount() == 1) {

        if (player->position() > 2000) {

            player->setPosition(0);

        }

    }

    return true;

}

bool MainWindow::on_volumeBar_valueChanged(int value) {

    player->setVolume(value);

    if (value == 0) {

        on_mute_clicked();
        mutedChanged();

    } else {

        if (player->isMuted()) {

            player->setMuted(false);

        }

    }

    return true;

}

bool MainWindow::mutedChanged() {

    if (!player->isMuted()) {

        if (ui->volumeBar->value() == 0) {

            ui->volumeBar->setValue(100);

        }

        ui->mute->setIcon(QIcon(MUTEICO));
        ui->mute->setChecked(false);

        switch (player->state()) {

            case QMediaPlayer::PlayingState :

                setWindowIcon(QIcon(WVOLICO));

                break;

            default :

                setWindowIcon(appIco);

                break;

        }

        if (QSystemTrayIcon::isSystemTrayAvailable()) {

            muteButtonAction->setIcon(QIcon(BMUTEICO));
            muteButtonAction->setText(tr("Mute"));
            trayIcon->setIcon(appIco);

        }

    } else {

        ui->mute->setIcon(QIcon(VOLICO));
        ui->mute->setChecked(true);

        setWindowIcon(QIcon(WMUTEICO));

        if (QSystemTrayIcon::isSystemTrayAvailable()) {

            muteButtonAction->setIcon(QIcon(BVOLICO));
            muteButtonAction->setText(tr("Sound"));
            trayIcon->setIcon(QIcon(WPHONESICO));

        }

    }

    return true;

}

bool MainWindow::on_mute_clicked() {

    (player->isMuted()) ? player->setMuted(false) : player->setMuted(true);

    return true;

}

bool MainWindow::on_repeat_clicked() {

    switch (playListObj->playbackMode()) {

        case QMediaPlaylist::CurrentItemOnce :

            if (playListObj->mediaCount() > 1) {

                playListObj->setPlaybackMode(QMediaPlaylist::Loop);

            } else {

                if (ui->repeat->isChecked() == Qt::Unchecked) {

                    ui->repeat->setChecked(false);

                } else {

                    ui->repeat->setChecked(true);
                    ui->shuffle->setChecked(false);

                }

            }

            break;

        default :

            if (playListObj->mediaCount() > 1) {

                    playListObj->setPlaybackMode(QMediaPlaylist::Loop);

                } else {

                    ui->repeat->setChecked(false);

                }

                playListObj->setPlaybackMode(QMediaPlaylist::CurrentItemOnce);

            break;

    }

    return true;

}

bool MainWindow::on_shuffle_clicked() {

        switch (playListObj->playbackMode()) {

            case QMediaPlaylist::Random :

                if (playListObj->mediaCount() > 1) {

                    playListObj->setPlaybackMode(QMediaPlaylist::Loop);

                } else {

                    ui->shuffle->setChecked(false);
                    playListObj->setPlaybackMode(QMediaPlaylist::CurrentItemOnce);

                }

                break;

            default :

                if (playListObj->mediaCount() > 1) {

                    playListObj->setPlaybackMode(QMediaPlaylist::Random);

                } else {

                    if (ui->shuffle->isChecked() == Qt::Unchecked) {

                        ui->shuffle->setChecked(false);

                    } else {

                        ui->shuffle->setChecked(true);
                        ui->repeat->setChecked(false);

                    }

                }

                break;

        }

    return true;

}

int MainWindow::getIndex() {

    return ui->tableView->currentIndex().row();

}

void MainWindow::keyPressEvent(QKeyEvent *event) {

    switch (event->key()) {

        case Qt::Key_Return : case Qt::Key_Enter : {

            if (playListObj->mediaCount() > 0) {

                QModelIndexList selection = ui->tableView->selectionModel()->selectedRows();

                for (int i=0; i< selection.count(); i++) {

                    QModelIndex index = selection.at(i);
                    ui->tableView->setCurrentIndex(index);
                    playListObj->setCurrentIndex(index.row());

                }

                ui->searchBar->clear();


                switch (player->state()) {

                    case QMediaPlayer::StoppedState :
                    case QMediaPlayer::PausedState :

                        player->play();
                        playIconPlay();

                        break;

                    default :

                        break;

                }

            }

            break;

        }

        case Qt::Key_Up : {

            int ind = getIndex() - 1;
            if (ind < 0) ind = playListObj->mediaCount() - 1;
            ui->tableView->selectRow(ind);

            break;

        }

        case Qt::Key_Down : {

            int ind = getIndex() + 1;
            if (ind >= playListObj->mediaCount()) ind = 0;
            ui->tableView->selectRow(ind);

            break;

        }

        case Qt::Key_Right : {

            on_forward_clicked();

            switch (player->state()) {

                case QMediaPlayer::StoppedState :
                case QMediaPlayer::PausedState :

                    player->play();
                    playIconPlay();

                    break;

                default :

                    break;

            }

            break;

        }

        case Qt::Key_Left : {

            on_back_clicked();

            switch (player->state()) {

                case QMediaPlayer::StoppedState :
                case QMediaPlayer::PausedState :

                    player->play();
                    playIconPlay();

                    break;

                default :

                    break;

            }

            break;

        }

        case Qt::Key_Delete : {

            on_remove_clicked();

            break;

        }

        case Qt::Key_Insert : {

            on_add_clicked();

            break;

        }

        case Qt::Key_Space : {

            on_play_clicked();

            break;

        }

       case Qt::Key_Escape : {

            QSettings settings("BOSS", "Player");
            escapeKeyExit = settings.value("application/esckeytoexit", "yes").toString().toLower();

            if (escapeKeyExit == "yes") {

                close();

            }

            break;

        }

        default : {

            ui->searchBar->setFocus();

            break;

        }

    }

    return QMainWindow::keyPressEvent(event); // Forward the event

}

bool MainWindow::forward() {

    if (ui->repeat->isChecked() == Qt::Unchecked) {

        playListObj->next();

    } else {

        QModelIndex ind = playListDataModel->index((getIndex() + 1), 0);
        ui->tableView->selectionModel()->clearSelection();
        ui->tableView->selectionModel()->setCurrentIndex(ind, QItemSelectionModel::Select);
        playListObj->setCurrentIndex(ind.row());

    }

    ui->searchBar->clear();

    return true;

}

bool MainWindow::back() {

    if (ui->repeat->isChecked() == Qt::Unchecked) {

        playListObj->previous();

    } else {

        QModelIndex ind = playListDataModel->index((getIndex() - 1), 0);
        ui->tableView->selectionModel()->clearSelection();
        ui->tableView->selectionModel()->setCurrentIndex(ind, QItemSelectionModel::Select);
        playListObj->setCurrentIndex(ind.row());

    }

    ui->searchBar->clear();

    return true;

}

void MainWindow::on_searchBar_textChanged(const QString &arg1) {

    if (arg1.isEmpty()) {

        return;

    } else {

        if (arg1.size() >= 2) {

            QModelIndexList matchList = ui->tableView->model()->match(ui->tableView->model()->index(0,0), Qt::EditRole, arg1, -1,  Qt::MatchFlags(Qt::MatchContains|Qt::MatchWrap));

            if (matchList.count() >= 1) {
     
                ui->tableView->setCurrentIndex(matchList.first());
                ui->tableView->scrollTo(matchList.first());

            }

        }

    }

}

bool MainWindow::dumpModelData() {

    QStringList itemList4;
    const int itemCount2 = playListDataModel->rowCount();

    if (itemCount2 >= 1) {

        for (int i = 0; i < itemCount2; ++i) {

            const QModelIndex indx = playListDataModel->index(i, 0);
            const QString itm = playListDataModel->data(indx).toString();

            if (!itm.isEmpty()) {

                itemList4.append(itm);

            } else {

                const QString emptyItem = "@EMPTY";
                itemList4.append(emptyItem);

            }

        }

        QFile fileExport2("DATAMODEL_ROW_0_DUMP.TXT");

        if (fileExport2.open(QFile::WriteOnly | QFile::Text)) {

            QTextStream t(&fileExport2);

            for (int b = 0; b < itemList4.size(); ++b) {

                t << itemList4.at(b) << '\n';

            }

            fileExport2.close();

        } else {

            ui->currentSong->setText("Export error. DATAMODEL_ROW_0_DUMP.TXT could not be opened to write to.");

            return false;

        }

    }

    return true;

}

bool MainWindow::dumpPlaylistData() {

    QStringList itemList;
    const int itemCount = playListObj->mediaCount();

    if (itemCount >= 1) {

        for (int i = 0; i < itemCount; ++i) {

            const QString item = playListObj->media(i).request().url().toLocalFile();

            if (!item.isEmpty()) {

                itemList.append(item);

            } else {

                const QString emptyItem = "This item was empty. It's number was: " + QString::number(i);
                itemList.append(emptyItem);

            }

        }

        QFile fileExport("PLAYLIST_DUMP.TXT");

        if (fileExport.open(QFile::WriteOnly | QFile::Text)) {

            QTextStream s(&fileExport);

            for (int i = 0; i < itemList.size(); ++i) {

                s << itemList.at(i) << '\n';

            }

            fileExport.close();

        } else {

            ui->currentSong->setText("Export error. File could not be opened.");

            return false;

        }

    }

    return true;

}

bool MainWindow::eventFilter(QObject *obj, QEvent *event) {

    const int index = getIndex();

    if (obj == (QObject*)ui->tableView->viewport()) {

        switch (event->type()) {

            case QEvent::DragMove :

                break;

            case QEvent::Drop : {

                ui->save->setChecked(true);

                break;

            }

            default :

                break;

        }

    } else {

#ifndef Q_OS_ANDROID
        
        const QString cT = playListObj->media(playListObj->currentIndex()).request().url().toString();
        const QFile fileA(QUrl(playListObj->media(playListObj->currentIndex()).request().url()).fileName());
        const QFileInfo fileInfo(fileA);
        const QString currentTrackName = trimFileExt(fileInfo.fileName());

        switch (event->type()) {

            case QEvent::HoverEnter : {

                messageTextTemp = currentTrackName;
                messagePreTrim = "";

                break;

            }

            case QEvent::HoverLeave : {

                messageTextTemp = currentTrackName;
                messagePreTrim = "";

                break;

            }

            default :

                break;

        }

        if (player->state() != QMediaPlayer::PlayingState) {

            switch (event->type()) {

                case QEvent::HoverEnter :

                    if (obj == (QObject*)ui->shuffle) {

                        (ui->shuffle->isChecked() == Qt::Unchecked) ? messageTextNew = tr("Shuffle playback") : messageTextNew = tr("Normal playback");

                    } else if (obj == (QObject*)ui->repeat) {

                        (ui->repeat->isChecked() == Qt::Unchecked) ? messageTextNew = tr("Repeat the current song") : messageTextNew = tr("Normal playback");

                    } else if (obj == (QObject*)ui->play) {

                        messageTextNew = tr("Play");

                    } else if (obj == (QObject*)ui->back) {

                        messageTextNew = tr("Back");

                    } else if (obj == (QObject*)ui->forward) {

                        messageTextNew = tr("Forward");

                    } else if (obj == (QObject*)ui->mute) {

                        if (ui->volumeBar->value() == 0) {

                            messageTextNew = tr("Increase the volume using the volume control");

                        } else {

                            (player->isMuted()) ? messageTextNew = tr("Make the sound audible") : messageTextNew = tr("Mute the volume");

                        }

                    } else if (obj == (QObject*)ui->save) {

                        messageTextNew = tr("Save the playlist");

                    } else if (obj == (QObject*)ui->about) {

                        messageTextNew = tr("About the application");

                    } else if (obj == (QObject*)ui->config) {

                        (config) ? messageTextNew = tr("Close configuration settings") : messageTextNew = tr("Open configuration settings");

                    } else if (obj == (QObject*)ui->empty) {

                        messageTextNew = tr("Empty the playlist");

                    } else if (obj == (QObject*)ui->remove) {

                        messageTextNew = tr("Remove a track");

                    } else if (obj == (QObject*)ui->add) {

                        messageTextNew = tr("Add to the playlist");

                    } else if (obj == (QObject*)ui->searchBar) {

                        messageTextNew = tr("Search");

                    } else if (obj == (QObject*)ui->volumeBar) {

                        messageTextNew = tr("Volume");

                    } else if (obj == (QObject*)ui->seekBar) {

                        messageTextNew = tr("Track position");

                    }

                    if (messageTextNew != "") {

                        ui->currentSong->setText(messageTextNew);

                    }

                    break;

                case QEvent::HoverLeave :

                    if (playListObj->mediaCount() < 1) {

                        messageTextTemp = tr("Add some files to the playlist to begin playback");
                        messageTextNew = "";

                    } else if (messageTextTemp == "") {

                        messageTextTemp = tr("You have found a bug!");
                        messageTextNew = "";

                    } else {

                        messageTextTemp = currentTrackName;
                        messagePreTrim = "";

                    }

                    ui->currentSong->setText(messageTextTemp);
                    messageTextTemp = "";
                    messageTextNew = "";

                    break;

                default :

                    break;

            }

        } else if (messageTextTemp == "") {

            switch (event->type()) {

                case QEvent::HoverLeave :

                    messageTextNew = currentTrackName;
                    ui->currentSong->setText(messageTextNew);
                    messageTextNew = "";
                    messagePreTrim = "";

                    break;

                default :

                    break;

            }

        }

#endif
  
    }
    
    return QMainWindow::eventFilter(obj, event); // Forward the event

}
