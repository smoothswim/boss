@extends('_includes.base')
@section('body')

    <div class="welcome">
        <div class="wrapper">
            <h1>{{ $siteName }}</h1>
            <header>
                <span>{{ $siteDescription }}</span>
            </header>
        </div>
    </div>
	<br />
    <div class="left-side"><main>
        @markdown
![BOSS Screen Capture](./assets/images/screencsp.png)

## Overview

BOSS Music player is an open source application for playing your music files.

### Project Goals

- Simplicity is key
- Easy to use
- A simple and lightly configurable user interface theme
- Helpful documentation
- Expanded cross platform testing and binary availability

### Documentation

- A user guide is available [here](https://gitlab.com/smoothswim/boss/-/blob/master/docs/src/User%20guide.pdf)

        @endmarkdown
    </main></div>

@stop