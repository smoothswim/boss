<section>
    <h3>Download</h3>
    <aside>
        <span>Grab the latest version here:</span>
        <br /><br />

        <div class="popover popover-bottom">
            <form style="display: inline" action="@url('assets/dl/'){{ $appArchive }}" method="get"><button class="btn btn-success">BOSS Music Player <i class="icon icon-link"></i></button></form>
            <div class="popover-container">
                <div class="card bg-dark">
                    <div class="card-header">
                        <h3 class="text-light">Warning:</h3>
                    </div>
                    <div class="card-body"><span class="text-warning bg-dark">This BOSS Music Player release is currently for 64 bit Microsoft Windows® 10 only.</span><br /><br /><span class="text-gray">Please <a href="https://gitlab.com/smoothswim/boss/issues" class="external-link">report</a> any bugs you find. <b>Thank You!</b></span></div>
                    <div class="card-footer"><i class="icon icon-download text-gray"></i></div>
                </div>
            </div>
        </div>

        <br /><br />
        <span>BOSS Music Player is licensed under the terms of the <a href="https://gitlab.com/smoothswim/boss/blob/src/license.txt" class="external-link">GNU General Public License v2</a></span>
        <br /><br />
        <span><a href="https://github.com/tabler/tabler-icons/" class="external-link">Tabler icons</a> are licensed under the terms of the <a href="https://gitlab.com/smoothswim/boss/blob/src/resources/LICENSE.TXT" class="external-link">MIT License</a></span>
        <br /><br />
        <span>The sha256 checksum for the file {{ $appArchive }} released on {{ $releaseDate }} is : </span>
        <br /><br />
        <div class="bg-dark" id="check-sum"><div class="text-bold"><span class="text-gray"><small>{{ $appChecksum }}</small></span></div></div>
        <br /><hr />

        <div class="column col-6"><br />
			<div class="card" id="card-support-gl">
				<div class="card-header">
					<div class="card-title h5">Hosted by</div>
				</div>
				<div class="card-image col-mx-auto"><a href="https://about.gitlab.com/" class="external-link"><img src="@url('assets/images/gitlab-logo-gray-rgb.svg')" class="img-responsive  width-128" alt="Gitlab" /></a></div>
				<div class="card-footer"><form style="display: inline" action="https://gitlab.com/smoothswim/boss" method="get"><button class="btn btn-primary">Fork the code</button></form></div>
			</div>
		</div>

        <br />
    </aside>
</section>