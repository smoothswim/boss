<!DOCTYPE html>
<html lang="en-GB">

	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="@yield('pageDescription', $siteDescription)" />

		<title>{{$siteName}} @yield('pageTitle')</title>

		<link rel="shortcut icon" href="@url('assets/images/favicon.ico')" type="image/x-icon" sizes="16x16 32x32"/>
		<link rel="stylesheet" href="@url('assets/css/spectre.min.css')" />
		<link rel="stylesheet" href="@url('assets/css/spectre-icons.min.css')" />
		<link rel="stylesheet" href="@url('assets/css/all.css')" />
	</head>

	<body>

		<div class="container">
			<div class="columns">
				<div class="column col-12"><div class="p-centered"><a href="@url('/')" class="internal-link"><img src="@url('assets/images/app.png')" class="img-responsive height-128" alt="BOSS Music Player Logo" /></a></div></div>
			</div>
		</div>

		<div class="container">
			<div class="columns">
				<div id="left-flex" class="column col-8">@yield('body')
					<div class="columns">
						<div class="column col-6">
						</div>
						<div class="column col-6"></div>
					</div>
				</div>
				<div id="right-flex" class="column col-6">
					<div class="right-side">@include('_includes.sidebar')</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="columns">
				<div class="column col-12">
					<div id="footer"><span class="text-dark"><small>All trademarks are copyright their various owners.</small></span></div>
				</div>
			</div>
		</div>

	</body>

</html>
