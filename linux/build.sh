#!/bin/bash
#
#
# Builds a binary of the application
#

cd ..
if [ -d "build" ]; then rm -rf build; fi
mkdir build && cd build
cmake ..
make
if [ -f BOSS ]; then ./BOSS; fi
cd ..
cd linux

exit
