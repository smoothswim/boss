
:: You should save this file to the name myvars.cmd
:: before you make any changes

SETLOCAL

:: Important Variables
::
SET SRCROOTDIR=D:\BOSS
::
:: End - Important Variables

:: VS2019 - Variables
::
SET TOOLSET=v142
SET MSVSDIR="D:\Programs\MSVS2019"
SET GENERATOR=-G "Visual Studio 16 2019" -A x64
::
:: End - VS2019 - Variables

:: Internal Variables
::
SET ANALYSIS=false
SET CONFIGURATION=release
SET VCPKGDIR=%SRCROOTDIR%\windows\vcpkg
SET QTDIR=%VCPKGDIR%\installed\x64-windows
SET INSTDIR=%SRCROOTDIR%\windows\binary
SET CMAKEBIN=cmake
SET VCRUN=%MSVSDIR%"\VC\Redist\MSVC\14.28.29325\x64\Microsoft.VC142.CRT\vcruntime140_1.dll"
::SET VCRUN=%MSVSDIR%"\VC\Redist\MSVC\14.28.29325\spectre\x64\Microsoft.VC142.CRT\vcruntime140_1.dll"
::
:: End - Internal Variables

IF exist %SRCROOTDIR%\ (  CALL %SRCROOTDIR%\windows\build.cmd )
