SETLOCAL

:: If the vcpkg portion of the very first build fails due to a download error
:: use this script to try the vcpkg part again. It should succeed and the
:: build script can then be run

:: Save this file as myfix.cmd and then
:: modify the SRCROOTDIR and MSVSDIR
:: variables before running this script

:: Important Variables
::
SET SRCROOTDIR=D:\BOSS
::
:: End - Important Variables

:: VS2019 - Variables
::
SET MSVSDIR="D:\Programs\MSVS2019"
::
:: End - VS2019 - Variables

:: Internal Variables
::
SET VCPKGDIR=%SRCROOTDIR%\windows\vcpkg
::
:: End - Internal Variables

IF exist %MSVSDIR%"\VC\Auxiliary\Build\vcvars64.bat" ( CALL %MSVSDIR%"\VC\Auxiliary\Build\vcvars64.bat" ) ELSE ( ECHO vcvars64.bat not found. Please ensure the correct path is set inside the variable MSVSDIR. && EXIT /B )

CD %VCPKGDIR%

vcpkg install qt5-base[latest]:x64-windows qt5-multimedia:x64-windows qt5-winextras:x64-windows qt5-tools:x64-windows qt5-translations:x64-windows --binarysource=clear

cd %SRCROOTDIR%\windows
