
:: This is the main build script.
:: The very first time this script is run there must be a working net connection.
:: 10 - 12 GB Free space available is required to build this application for Windows 10.

IF exist %VCRUN% ( ECHO vcruntime140_1 found. ) ELSE ( ECHO vcruntime140_1.dll not found. Please ensure the correct path is set inside the variable VCRUN. && EXIT /B )

IF exist %MSVSDIR%"\VC\Auxiliary\Build\vcvars64.bat" ( CALL %MSVSDIR%"\VC\Auxiliary\Build\vcvars64.bat" ) ELSE ( ECHO vcvars64.bat not found. Please ensure the correct path is set inside the variable MSVSDIR. && EXIT /B )

cd %SRCROOTDIR%

IF exist %VCPKGDIR% (  GOTO :cleanup )

:setupvcpkg

:: WARNING: vcpkg now uses C:\Users\%username%\AppData\Local\vcpkg\ to cache binaries by default!
:: You might want to delete them afterwards. I struggle to understand the change, as most
:: build machines have their OS's on SSD drives and build using completely separate drives.
MKDIR %VCPKGDIR% && CD %VCPKGDIR% && git init
git fetch https://github.com/Microsoft/vcpkg master
:: Last update to vcpkg HEAD: 07/02/2021
git merge 5eea585548058540c3b73d087eb101c033b5fa85
echo.set(VCPKG_BUILD_TYPE %CONFIGURATION%)>> %VCPKGDIR%\triplets\x64-windows.cmake
echo.set(VCPKG_BUILD_TYPE %CONFIGURATION%)>> %VCPKGDIR%\triplets\x64-windows-static.cmake
:: Fix the use of /Z7 when creating release binaries
powershell -command " & {(Get-Content %VCPKGDIR%\scripts\toolchains\windows.cmake).replace('/DNDEBUG /Z7', '/DNDEBUG /Qspectre /Qpar /Ot /MP /arch:AVX') | Set-Content %VCPKGDIR%\scripts\toolchains\windows.cmake}"
powershell -command " & {(Get-Content %VCPKGDIR%\scripts\cmake\vcpkg_configure_meson.cmake).replace('/DNDEBUG /Z7', '/DNDEBUG /Qspectre /Qpar /Ot /MP /arch:AVX') | Set-Content %VCPKGDIR%\scripts\cmake\vcpkg_configure_meson.cmake}"

CALL .\bootstrap-vcpkg.bat -disableMetrics
vcpkg install qt5-base[latest]:x64-windows qt5-multimedia:x64-windows qt5-winextras:x64-windows qt5-tools:x64-windows qt5-translations:x64-windows --binarysource=clear
:: To remove the Qt packages
:: vcpkg remove --recurse qt5-base:x64-windows

:cleanup

IF exist %SRCROOTDIR%\build\ ( RMDIR /Q /S "%SRCROOTDIR%\build\" ) ELSE ( ECHO stale build directory not found )
IF exist %INSTDIR% ( RMDIR /Q /S "%INSTDIR%" ) ELSE ( ECHO stale binary directory not found )

:build

mkdir %SRCROOTDIR%\build && cd %SRCROOTDIR%\build

%CMAKEBIN% .. %GENERATOR% ^
-Duse_mp:BOOL="1" ^
-Duse_o2:BOOL="1" ^
-Duse_ot:BOOL="1" ^
-Duse_gy:BOOL="1" ^
-Duse_oi:BOOL="1" ^
-Duse_avx2:BOOL="1" ^
-Duse_qpar:BOOL="1" ^
-Duse_qspectre:BOOL="1" ^
-Duse_control_flow_guard:BOOL="1" ^
-DQt5_DIR:PATH="%VCPKGDIR%\packages\qt5-base_x64-windows\share\cmake\Qt5" ^
-DQt5Network_DIR:PATH="%VCPKGDIR%\packages\qt5-base_x64-windows\share\cmake\Qt5Network" ^
-DQt5Multimedia_DIR:PATH="%VCPKGDIR%\packages\qt5-multimedia_x64-windows\share\cmake\Qt5Multimedia" ^
-DQt5WinExtras_DIR:PATH="%VCPKGDIR%\packages\qt5-winextras_x64-windows\share\cmake\Qt5WinExtras"

msbuild /m ".\BOSS.sln" /p:CharacterSet=Unicode /p:configuration=%CONFIGURATION% /p:platform=x64 /p:PlatformToolset=%TOOLSET% /p:RunCodeAnalysis=%ANALYSIS% /p:PreferredToolArchitecture=x64 /p:UseEnv=true

MKDIR %INSTDIR%

COPY %VCPKGDIR%\packages\zlib_x64-windows\bin\zlib1.dll %INSTDIR%
COPY %VCPKGDIR%\packages\pcre2_x64-windows\bin\pcre2-16.dll %INSTDIR%
COPY %VCPKGDIR%\packages\libpng_x64-windows\bin\libpng16.dll %INSTDIR%
COPY %VCPKGDIR%\packages\harfbuzz_x64-windows\bin\harfbuzz.dll %INSTDIR%
COPY %VCPKGDIR%\packages\freetype_x64-windows\bin\freetype.dll %INSTDIR%
COPY %VCPKGDIR%\packages\bzip2_x64-windows\bin\bz2.dll %INSTDIR%

COPY %VCRUN% %INSTDIR%
COPY %SRCROOTDIR%"\docs\src\User guide.pdf" %INSTDIR%
COPY %SRCROOTDIR%\build\Release\BOSS.exe %INSTDIR%
COPY %SRCROOTDIR%\src\license.txt %INSTDIR%

SET PATH=%PATH%;%QTDIR%\bin;%QTDIR%\tools\qt5\bin

%QTDIR%\tools\qt5-tools\bin\windeployqt ^
-winextras ^
--libdir %INSTDIR% ^
--plugindir %INSTDIR% ^
--release ^
--no-opengl-sw ^
--no-angle ^
--no-compiler-runtime ^
%INSTDIR%

cd %SRCROOTDIR%\windows

ECHO -
IF exist "%INSTDIR%\BOSS.exe" (
	IF exist "%INSTDIR%\Qt5Multimedia.dll" (
		ECHO -
		ECHO - [32mBuild completed successfully![0m
		START /D "%INSTDIR%" BOSS.exe
	) ELSE (
		ECHO -
		ECHO - [31mBuild failure[0m - Please review the console output
	)
) ELSE (
	ECHO -
	ECHO - [31mBuild failure[0m - Please review the console output
)
