
:: Gitlab CI Windows Native build

SETLOCAL

:: Important Variables
::
SET SRCROOTDIR=C:\GitLab-Runner\builds\smoothswim\boss
::
:: End - Important Variables

:: VS2019 - Variables
::
SET TOOLSET=v142
SET MSVSDIR="C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools"
SET GENERATOR=-G "Visual Studio 16 2019" -A x64
::
:: End - VS2019 - Variables

:: Internal Variables
::
SET ANALYSIS=false
SET CONFIGURATION=release
SET INSTDIR=%SRCROOTDIR%\windows\binary
:: The correct folder directory (eg 14.26.28801) can be found via 'DIR %MSVSDIR%\VC\Tools\MSVC\'
SET VCRUN=%MSVSDIR%\VC\Tools\MSVC\14.26.28801\bin\Hostx64\x64\vcruntime140_1.dll
:: Use a mirror here because https://download.qt.io/online/qtsdkrepository/windows_x86/desktop is flaky
SET BASEURLQT=https://ftp1.nluug.nl/languages/qt/online/qtsdkrepository/windows_x86/desktop
SET QTURL=%BASEURLQT%/qt5_5152
SET EXTRASTRING=qt.qt5.5152.win64_msvc2019_64/5.15.2-0-202011130602
SET QTDIR=%SRCROOTDIR%\qt\5.15.2\msvc2019_64
SET CMAKEBIN=%SRCROOTDIR%\cmake-3.19.2-win64-x64\bin\cmake.exe
::
:: End - Internal Variables

IF exist %VCRUN% ( ECHO vcruntime140_1 found. ) ELSE ( ECHO vcruntime140_1.dll not found. Please ensure the correct path is set inside the variable VCRUN. && DIR %MSVSDIR%\VC\Tools\MSVC\ && powershell -command "throw 'vcruntime140_1.dll not found.'" && GOTO :buildend )

IF exist %SRCROOTDIR%\qt\ (  GOTO :begin )
powershell -command "Invoke-WebRequest -Uri https://www.7-zip.org/a/7za920.zip -OutFile 7za920.zip"
powershell -command "Expand-Archive 7za920.zip -DestinationPath C:\GitLab-Runner\builds\smoothswim\boss\7z\ -Force"
powershell -command "Invoke-WebRequest -Uri https://github.com/Kitware/CMake/releases/download/v3.19.2/cmake-3.19.2-win64-x64.zip -OutFile C:\cmake.zip"
%SRCROOTDIR%\7z\7za x C:\cmake.zip -aoa -o%SRCROOTDIR%\

powershell -command "Invoke-WebRequest -Uri %QTURL%/%EXTRASTRING%qtactiveqt-Windows-Windows_10-MSVC2019-Windows-Windows_10-X86_64.7z -OutFile C:\qtactiveqt.7z"
%SRCROOTDIR%\7z\7za x C:\qtactiveqt.7z -aoa -o%SRCROOTDIR%\qt
powershell -command "Invoke-WebRequest -Uri %QTURL%/%EXTRASTRING%qtbase-Windows-Windows_10-MSVC2019-Windows-Windows_10-X86_64.7z -OutFile C:\qtbase.7z"
%SRCROOTDIR%\7z\7za x C:\qtbase.7z -aoa -o%SRCROOTDIR%\qt
powershell -command "Invoke-WebRequest -Uri %QTURL%/%EXTRASTRING%qtdeclarative-Windows-Windows_10-MSVC2019-Windows-Windows_10-X86_64.7z -OutFile C:\qtdeclarative.7z"
%SRCROOTDIR%\7z\7za x C:\qtdeclarative.7z -aoa -o%SRCROOTDIR%\qt
powershell -command "Invoke-WebRequest -Uri %QTURL%/%EXTRASTRING%qtimageformats-Windows-Windows_10-MSVC2019-Windows-Windows_10-X86_64.7z -OutFile C:\qtimageformats.7z"
%SRCROOTDIR%\7z\7za x C:\qtimageformats.7z -aoa -o%SRCROOTDIR%\qt
powershell -command "Invoke-WebRequest -Uri %QTURL%/%EXTRASTRING%qtmultimedia-Windows-Windows_10-MSVC2019-Windows-Windows_10-X86_64.7z -OutFile C:\qtmultimedia.7z"
%SRCROOTDIR%\7z\7za x C:\qtmultimedia.7z -aoa -o%SRCROOTDIR%\qt
powershell -command "Invoke-WebRequest -Uri %QTURL%/%EXTRASTRING%qtsvg-Windows-Windows_10-MSVC2019-Windows-Windows_10-X86_64.7z -OutFile C:\qtsvg.7z"
%SRCROOTDIR%\7z\7za x C:\qtsvg.7z -aoa -o%SRCROOTDIR%\qt
powershell -command "Invoke-WebRequest -Uri %QTURL%/%EXTRASTRING%qttools-Windows-Windows_10-MSVC2019-Windows-Windows_10-X86_64.7z -OutFile C:\qttools.7z"
%SRCROOTDIR%\7z\7za x C:\qttools.7z -aoa -o%SRCROOTDIR%\qt
powershell -command "Invoke-WebRequest -Uri %QTURL%/%EXTRASTRING%qttranslations-Windows-Windows_10-MSVC2019-Windows-Windows_10-X86_64.7z -OutFile C:\qttranslations.7z"
%SRCROOTDIR%\7z\7za x C:\qttranslations.7z -aoa -o%SRCROOTDIR%\qt
powershell -command "Invoke-WebRequest -Uri %QTURL%/%EXTRASTRING%qtwinextras-Windows-Windows_10-MSVC2019-Windows-Windows_10-X86_64.7z -OutFile C:\qtwinextras.7z"
%SRCROOTDIR%\7z\7za x C:\qtwinextras.7z -aoa -o%SRCROOTDIR%\qt
powershell -command "Invoke-WebRequest -Uri %QTURL%/%EXTRASTRING%d3dcompiler_47-x64.7z -OutFile C:\d3dcompiler.7z"
%SRCROOTDIR%\7z\7za x C:\d3dcompiler.7z -aoa -o%SRCROOTDIR%\qt
powershell -command "Invoke-WebRequest -Uri %QTURL%/%EXTRASTRING%opengl32sw-64-mesa_12_0_rc2.7z -OutFile C:\opengl32sw.7z"
%SRCROOTDIR%\7z\7za x C:\opengl32sw.7z -aoa -o%SRCROOTDIR%\qt

:begin

CALL %MSVSDIR%"\VC\Auxiliary\Build\vcvars64.bat"

cd %SRCROOTDIR% && mkdir %SRCROOTDIR%\build && cd %SRCROOTDIR%\build

%CMAKEBIN% .. %GENERATOR% ^
-Duse_mp:BOOL="1" ^
-Duse_o2:BOOL="1" ^
-Duse_ot:BOOL="1" ^
-Duse_gy:BOOL="1" ^
-Duse_oi:BOOL="1" ^
-Duse_avx2:BOOL="1" ^
-Duse_qpar:BOOL="1" ^
-Duse_qspectre:BOOL="0" ^
-Duse_control_flow_guard:BOOL="1" ^
-DQt5_DIR:PATH="%QTDIR%\lib\cmake\Qt5" ^
-DQt5Network_DIR:PATH="%QTDIR%\lib\cmake\Qt5Network" ^
-DQt5Multimedia_DIR:PATH="%QTDIR%\lib\cmake\Qt5Multimedia" ^
-DQt5WinExtras_DIR:PATH="%QTDIR%\lib\cmake\Qt5WinExtras"

%MSVSDIR%\MSBuild\Current\Bin\msbuild /m ".\BOSS.sln" /p:CharacterSet=Unicode /p:configuration=%CONFIGURATION% /p:platform=x64 /p:PlatformToolset=%TOOLSET% /p:RunCodeAnalysis=%ANALYSIS% /p:PreferredToolArchitecture=x64 /p:UseEnv=true

MKDIR %INSTDIR%

COPY %VCRUN% %INSTDIR%
COPY %SRCROOTDIR%"\docs\src\User guide.pdf" %INSTDIR%
COPY %SRCROOTDIR%\build\Release\BOSS.exe %INSTDIR%
COPY %SRCROOTDIR%\src\license.txt %INSTDIR%

SET PATH=%PATH%;%QTDIR%

%QTDIR%\bin\windeployqt ^
-winextras ^
--libdir %INSTDIR% ^
--plugindir %INSTDIR% ^
--release ^
--no-opengl-sw ^
--no-angle ^
--no-compiler-runtime ^
%INSTDIR%

RENAME %INSTDIR% "Boss"

powershell -command "Compress-Archive -Path 'C:\GitLab-Runner\builds\smoothswim\boss\windows\Boss' -DestinationPath 'C:\GitLab-Runner\builds\smoothswim\boss\Boss.zip'"

:buildend
