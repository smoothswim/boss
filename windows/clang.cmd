
:: Experimental clang Windows build test script
:: (ninja.exe and clang (llvm) must acessible via the PATH)
:: This currently works but produces a buggy application


:: Please DO NOT edit

SETLOCAL

:: Important Variables
::
SET SRCROOTDIR=D:\BOSS
::
:: End - Important Variables

:: VS2019 - Variables
::
SET TOOLSET=v142
SET MSVSDIR="D:\Programs\MSVS2019"
SET GENERATOR=-G Ninja
::
:: End - VS2019 - Variables

:: Clang - Variables
::
SET CC=clang-cl.exe
SET CXX=clang-cl.exe
::
:: End - Clang - Variables

:: Internal Variables
::
SET VCPKGDIR=%SRCROOTDIR%\windows\vcpkg
SET QTDIR=%VCPKGDIR%\installed\x64-windows
SET INSTDIR=%SRCROOTDIR%\windows\binary
SET CMAKEBIN=cmake
SET VCRUN=%MSVSDIR%"\VC\Redist\MSVC\14.28.29325\x64\Microsoft.VC142.CRT\vcruntime140_1.dll"
::SET VCRUN=%MSVSDIR%"\VC\Redist\MSVC\14.28.29325\spectre\x64\Microsoft.VC142.CRT\vcruntime140_1.dll"
::
:: End - Internal Variables

IF exist %VCRUN% ( ECHO vcruntime140_1 found. ) ELSE ( ECHO vcruntime140_1.dll not found. Please ensure the correct path is set inside the variable VCRUN. && EXIT /B )

IF exist %MSVSDIR%"\VC\Auxiliary\Build\vcvars64.bat" ( CALL %MSVSDIR%"\VC\Auxiliary\Build\vcvars64.bat" ) ELSE ( ECHO vcvars64.bat not found. Please ensure the correct path is set inside the variable MSVSDIR. && EXIT /B )

cd %SRCROOTDIR%

IF exist %SRCROOTDIR%\build\ ( RMDIR /Q /S "%SRCROOTDIR%\build\" ) ELSE ( ECHO stale build directory not found )
IF exist %INSTDIR% ( RMDIR /Q /S "%INSTDIR%" ) ELSE ( ECHO stale binary directory not found )

:build

mkdir %SRCROOTDIR%\build && cd %SRCROOTDIR%\build

%CMAKEBIN% .. %GENERATOR% ^
-Duse_mp:BOOL="0" ^
-Duse_o2:BOOL="0" ^
-Duse_ot:BOOL="0" ^
-Duse_gy:BOOL="0" ^
-Duse_oi:BOOL="0" ^
-Duse_avx2:BOOL="0" ^
-Duse_qpar:BOOL="0" ^
-Duse_qspectre:BOOL="0" ^
-Duse_control_flow_guard:BOOL="0" ^
-DQt5_DIR:PATH="%VCPKGDIR%\packages\qt5-base_x64-windows\share\cmake\Qt5" ^
-DQt5Network_DIR:PATH="%VCPKGDIR%\packages\qt5-base_x64-windows\share\cmake\Qt5Network" ^
-DQt5Multimedia_DIR:PATH="%VCPKGDIR%\packages\qt5-multimedia_x64-windows\share\cmake\Qt5Multimedia" ^
-DQt5WinExtras_DIR:PATH="%VCPKGDIR%\packages\qt5-winextras_x64-windows\share\cmake\Qt5WinExtras"

ninja

MKDIR %INSTDIR%

COPY %VCPKGDIR%\packages\zlib_x64-windows\bin\zlib1.dll %INSTDIR%
COPY %VCPKGDIR%\packages\pcre2_x64-windows\bin\pcre2-16.dll %INSTDIR%
COPY %VCPKGDIR%\packages\libpng_x64-windows\bin\libpng16.dll %INSTDIR%
COPY %VCPKGDIR%\packages\harfbuzz_x64-windows\bin\harfbuzz.dll %INSTDIR%
COPY %VCPKGDIR%\packages\freetype_x64-windows\bin\freetype.dll %INSTDIR%
COPY %VCPKGDIR%\packages\bzip2_x64-windows\bin\bz2.dll %INSTDIR%

COPY %VCRUN% %INSTDIR%
COPY %SRCROOTDIR%"\docs\src\User guide.pdf" %INSTDIR%
:: The location of BOSS.exe is different when ninja is used
COPY %SRCROOTDIR%\build\BOSS.exe %INSTDIR%
COPY %SRCROOTDIR%\src\license.txt %INSTDIR%

SET PATH=%PATH%;%QTDIR%\bin;%QTDIR%\tools\qt5\bin

%QTDIR%\tools\qt5-tools\bin\windeployqt ^
-winextras ^
--libdir %INSTDIR% ^
--plugindir %INSTDIR% ^
--release ^
--no-opengl-sw ^
--no-angle ^
--no-compiler-runtime ^
%INSTDIR%

cd %SRCROOTDIR%\windows

ECHO -
IF exist "%INSTDIR%\BOSS.exe" (
	IF exist "%INSTDIR%\Qt5Multimedia.dll" (
		ECHO - [32mBuild completed successfully![0m ) ELSE (
		ECHO - [31mBuild failure[0m - Please review the console output
	)
) ELSE (
	ECHO - [31mBuild failure[0m - Please review the console output
)
