
cmake_minimum_required(VERSION 3.17)

project(BOSS VERSION 0.0.4)

set(CMAKE_CXX_STANDARD 14)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Instruct CMake to run moc automatically when needed.
# To automatically run MOC when building (Meta Object Compiler)
set(CMAKE_AUTOMOC ON)
# To automatically run UIC when building (User Interface Compiler)
set(CMAKE_AUTOUIC ON)
# To automatically run RCC when building (Resource Compiler)
set(CMAKE_AUTORCC ON)

set(QT_MIN_VERSION "5.14.2")

find_package(Qt5 ${QT_MIN_VERSION} REQUIRED COMPONENTS Core Widgets Multimedia)

if (WIN32)
	include(${CMAKE_SOURCE_DIR}/windows/msvc.conf)
	find_package(Qt5 ${QT_MIN_VERSION} REQUIRED COMPONENTS WinExtras) # Add "Tools" here too for windeployqt, althought the build does not depend upon them
    # https://docs.microsoft.com/en-us/cpp/porting/modifying-winver-and-win32-winnt?view=vs-2019
    add_definitions(-DWINVER=0x0A00)
    add_definitions(-D_WIN32_WINNT=0x0A00)
endif()

if (UNIX AND NOT APPLE AND NOT NO_X11 AND NOT ANDROID)
    add_definitions(-DQZ_WS_X11)
    find_package(Qt5 ${QT_MIN_VERSION} REQUIRED COMPONENTS X11Extras)
endif()

if (APPLE)
    find_package(Qt5 ${QT_MIN_VERSION} REQUIRED COMPONENTS MacExtras)
endif()

if (ANDROID)
    find_package(Qt5 ${QT_MIN_VERSION} REQUIRED COMPONENTS AndroidExtras)
endif()

set(SOURCES ${CMAKE_SOURCE_DIR}/src/main.cpp
			${CMAKE_SOURCE_DIR}/src/mainwindow.cpp
) 

set(HEADERS ${CMAKE_SOURCE_DIR}/src/mainwindow.h)

set(UI ${CMAKE_SOURCE_DIR}/src/mainwindow.ui)

if (NOT ANDROID)
	qt5_add_resources(RES ${CMAKE_SOURCE_DIR}/src/icons.qrc)
endif()

if (WIN32)
    if (DEBUG)
        add_executable(BOSS ${SOURCES} ${HEADERS} ${UI} ${RES} ${CMAKE_SOURCE_DIR}/src/ver.rc) # Removed WIN32 for debug
    else ()
        add_executable(BOSS WIN32 ${SOURCES} ${HEADERS} ${UI} ${RES} ${CMAKE_SOURCE_DIR}/src/ver.rc)
    endif()
elseif (ANDROID)
    add_library(BOSS SHARED ${SOURCES} ${HEADERS} ${UI})
else ()
    add_executable(BOSS ${SOURCES} ${HEADERS} ${UI} ${RES})
endif()

if (DEBUG)
    # Add some debug things here
else ()
    set(UNITY_BUILD true)
    target_precompile_headers(BOSS PRIVATE ${CMAKE_SOURCE_DIR}/src/mainwindow.h)
endif()

target_link_libraries(BOSS Qt5::Widgets Qt5::Multimedia)

if (WIN32)
    target_link_libraries(BOSS Qt5::WinExtras)
endif()

if (UNIX AND NOT APPLE AND NOT NO_X11 AND NOT ANDROID)
    target_link_libraries(BOSS Qt5::X11Extras)
endif()

if (APPLE)
    target_link_libraries(BOSS Qt5::MacExtras)
endif()

# https://github.com/OlivierLDff/QtAndroidCMake

if (ANDROID)
    target_link_libraries(BOSS Qt5::AndroidExtras)
    include(${CMAKE_SOURCE_DIR}/android/QtAndroidCMake/AddQtAndroidApk.cmake)
    add_qt_android_apk(BOSS_apk BOSS
	NAME "BOSS"
	VERSION_CODE 1
	PACKAGE_NAME "org.company.boss"
	PACKAGE_SOURCES ${CMAKE_SOURCE_DIR}/android/package-sources
	KEYSTORE /build/android-keystore/keystore.jks
	KEY_ALIAS signed-key
	KEYSTORE_PASSWORD keystore
	KEY_PASSWORD keystore
	)
endif()

