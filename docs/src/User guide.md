

![BOSS Logo](../images/screens/app-black.png)

# BOSS Music Player Documentation - User Guide

## _[smoothswim.gitlab.io/boss](https://smoothswim.gitlab.io/boss/)_

<div class="page-break"></div>

### Topics:

This document covers the following :

* **Overview**
* **Obtaining a copy of the program**
* **The basics**
* **File format support**
* **The playlist**
* **Shortcut keys**
* **Settings**
* **Tray icon**
* **Command line options**
* **Reporting bugs**

The topics covered are shown in the order that they are listed for easy reference.


###  Overview

This document's aim is to guide you through using **BOSS Music Player** whilst exposing the application's feature set to you as we go.
The information is broken into sections for ease of reference and aims to hopefully be as concise and as helpful as possible.

If you have any suggestions or recommendations of how to improve this document, please propose a merge request with your changes via [Gitlab](https://gitlab.com/smoothswim/boss/issues).
Your help is greatly appreciated.


### Obtaining a copy of the program

For Windows 10, compiled versions of the program are available from the [Home page](https://smoothswim.gitlab.io/boss/). Once you have downloaded a copy, simply extract the archive file into a location somewhere on your computer and then run the file **BOSS.EXE**.

For platforms other than Windows 10, you can compile the [source code](https://gitlab.com/smoothswim/boss).

<div class="page-break"></div>

### The basics

![BOSS Main application image](../images/screens/labelled.png)

<div class="page-break"></div>

**1.** Playlist display
**2.** Repeat
**3.** Shuffle
**4.** Remove an item from the playlist
**5.** Add item(s) to the playlist
**6.** Clear the playlist entirely
**7.** Search bar
**8.** Check for updates
**9.** Application Settings
**10.** Volume slider control
**11.** Mute toggle switch
**12.** Save the playlist
**13.** Status information
**14.** Track progress slider control
**15.** Play the previous track
**16.** Play/Pause toggle switch
**17.** Play the next track

When you run the program for the very first time, the main window will appear revealing the playlist and the player controls. The playlist will be empty.

#### The controls

**1.** _Playlist display_ - Tracks are shown as individual items within the playlist once they have been added. Items in the playlist can be re-ordered using drag and drop.

**2.** _Repeat_ - This button toggles whether the currently playing audio track will repeat indefinitely.

**3.** _Shuffle_ - This button toggles the shuffle feature on and off. In shuffle mode, a random track from the playlist will be selected to play instead of the next track in the playlist in the event that the current track ends or the next track is selected. When not in shuffle or repeat mode, tracks are played sequentially in the order at which they are shown within the playlist.

**4.** _Remove an item from the playlist_ - Selecting an item in the playlist by clicking on it and then clicking on this button will remove the item from the playlist.

**5.** _Add item(s) to the playlist_ - Clicking on this button presents the option to add music files to the playlist. Several files can be added at once.

**6.** _Clear the playlist entirely_ - This button will completely empty the current playlist. Doing so will not alter the saved copy of the default playlist unless the save playlist button is then subsequently clicked.

**7.** _The search bar_ - Type in the name of a track you want to find within the current playlist. The selection emphasis should change to highlight the track you were searching for. Press the enter key to play the newly selected track.

**8.** _Check for updates_ - This button launches the home page if you wanted to check for an application update.

**9.** _Application Settings_ - Clicking on this button reveals the application's settings.

**10.** _Volume slider control_ - The volume control allows you to change how loud or quiet the playback audio is. The volume is quietest and eventually mute, the further left the slider is moved. The volume becomes louder the further right the slider is moved.

**11.** _Mute toggle switch_ - Clicking on the mute button will toggle whether the sound can be heard or not.

**12.** _Save the playlist_ - This button will save the current playlist to the default playlist file. When there are unsaved changes to the playlist, the button will change colour and appear highlighted. The playlist does not automatically save at any given point.

**13.** _Status information_ - When paused, this display area will give you hints detailing the function of each button. When audio is playing, the display shows the currently playing file name.

**14.** _Track progress slider control_ - This control shows the overall playing position of the currently playing audio file. The progress indicator can be moved forward and backwards to change the playback position.

**15.** _Play the previous track_ - This control selects and plays the previous track in the playlist.

**16.** _Play/Pause toggle switch_ - This button starts or stops the playback of the currently loaded audio track.

**17.** _Play the next track_ - This control selects and plays the next track in the playlist. By default in sequential order but in shuffle mode a random track will be played instead. When in repeat mode, the currently playing song will play again, starting from the beginning.

The volume, mute, shuffle and repeat features are all persistent and remain the same even after the application has been closed. This means that if you mute the application and then close it, when you next open the application, the volume will initially be muted.

Whilst the player is paused, hovering the mouse cursor over any of the button controls will show a helpful action hint within the status information display.


### File format support

The availability of audio file format support within BOSS Music Player depends upon the operating system. BOSS Music Player does not decode music files. Most operating systems support wav and mp3 by default but some may support more such as wma, ogg or flac.

On Windows 10, the available formats are mostly covered [here](https://docs.microsoft.com/en-us/windows/win32/directshow/supported-formats-in-directshow). Unfortunately MP4 Audio (.m4a) does not play back and is disabled. To enable ogg, weba, spx and flac format playback on Windows 10, please install [xiph.org's directshow filters](https://xiph.org/dshow/downloads/).

On Android, the audio file format support is listed in the [developer documentation](https://developer.android.com/guide/topics/media/media-formats#audio-codecs).

On Linux, audio file format support commonly revolves around the gstreamer plugins which have been installed system wide.


### The playlist

![BOSS Main playlist image](../images/screens/playlist.png)

The playlist displays the music files cued up and ready to go; in the order they were added. To add some files to the playlist, click on the button showing the __```+```__ icon.

When playback is paused, items in the playlist can be re-ordered using drag and drop. Each one can be moved to a new position. This feature is currently experimental.

When there are unsaved changes within the playlist, the save playlist button will become highlighted. Changes to the playlist are never automatically saved. Click on the save playlist button to manually save the playlist.

To remove a song from the playlist, click on the song name and then click on the button showing the __```-```__ icon.
To completely empty the playlist remove all songs, press the button with the __```x```__ icon.

The playlist plays songs in a sequential order unless either the shuffle or the repeat mode has been selected.

In shuffle mode the playlist will play back songs in a random order.

The repeat mode will completely ignore the playlist and playback only the current track on a loop, repeatedly until the mode is disabled.

<div class="page-break"></div>

### Shortcut keys

The application supports the following short cut keys:

__```RIGHT ARROW```__

_Play the next track_

__```LEFT ARROW```__

_Play the previous track_

__```SPACE```__

_Play or pause the song_

__```UP ARROW```__

_Move the highlighted playlist position up by one place_

__```DOWN ARROW```__

_Move the highlighted playlist position down by one place_

__```ENTER```__

_Play the currently selected track immediately_

__```ESC```__

_Quit the application_
<div class="page-break"></div>

### Settings

Clicking on the settings button (**...**) reveals the configuration settings menu.

![BOSS settings image](../images/screens/settings.png)

The available settings are :

- **Theme Main Colour** - Dynamically switch the application theme's main colour.
- **Primary Text Colour** - Dynamically switch the application theme's primary text colour.
- **Background Colour** - Dynamically switch the application theme's background colour.
- **Highlight Colour** - Dynamically switch the application theme's highlight colour.
- **Button Colour** - Dynamically switch the application theme's button colour.
- **Reset Colours** - Change all of the theme colours back to their original defaults.
- **Use native OS theme** - Change the application to use the default operating system theme style instead of applying any theme style.
- **Enable the application tray icon** - Show or hide the application's system tray icon.
- **Load the default playlist at start up** - Enable the application to automatically load the default playlist when the application opens.
- **Playlists will play automatically** - Enable a playlist to play automatically once it has been loaded.
- **Enable ESC key to exit** - Enable exiting the application by pressing the __```ESC```__ shortcut key.

All of the theme settings apply immediately once a new setting has been chosen. All the other settings are only applied after the settings are closed.

To close the configuration settings, click once more on the settings button (**...**).

The changes you make to the configuration settings are persistent and will remain even after the application has been closed.

<div class="page-break"></div>

### Tray icon

![BOSS tray image](../images/screens/tray.png)

On operating systems which support it, BOSS Music Player creates a tray icon in the system tray. This option can be disabled from the configuration settings.

When the tray icon is enabled, the application can minimised to it. The tray icon also provides a quick action menu, which can be used optionally to control the application.

To minimise BOSS Music Player to the system tray, left click on the application's icon in the task bar. Afterwards, left click on the same icon again to then show the application's main Window once more.

![BOSS tray menu image](../images/screens/traymenu.png)

The tray icon menu allows you to mute the volume, pause or play the current song, control the playback transport (back or forward) or quit the application altogether. A single left click on one of the items found within the tray menu will trigger the item's advertised action.

<div class="page-break"></div>

#### Revealing the icon on Windows 10

![Windows taskbar settings image](../images/screens/taskbarsettings.png)

If you have set "_Always show all icons in the notification area_" to off and you cannot see the tray icon when using Windows 10, if the "_Enable the application tray icon_" setting in BOSS Music Player is also enabled, you can manually enable the icon if you want to use it's functionality.

**To show the application icon when using Windows 10** :

Combine the __```WINDOWS```__ key and the __```I```__ key to bring up the Windows settings application (_Alternatively, right click on the dektop's start menu icon and then left click on the settings menu item._)

Using the search bar, search for the term : "_Select which icons appear on the taskbar_" then click on the result which appears (_alternatively, click on "Personalisation" click on "taskbar" then scroll down to and click on "Select which icons appear on the taskbar."_)

Finally, scroll down the list of applications until you see the entry for **BOSS.exe** and then toggle the flip switch by left clicking on it once. The setting should now be **On** and the tray icon will from then on, always appear when the application is running.

<div class="page-break"></div>

### Command line options

Usage:

**BOSS.EXE** [filename]

Where **[filename]** the full path and file name of:

A supported audio file.

Or

An m3u playlist file.

_Examples:_

- The code below shows how to launch the application and then play a single audio file or add an m3u playlist.

__``` BOSS.EXE C:\happy-song.wav```__

__``` BOSS.EXE "C:\happy playlist.m3u"```__

_Replace **C:\happy-song.wav** or **C:\happy playlist.m3u** with the file you want to use instead._

<div class="page-break"></div>

### Reporting bugs

Care has been taken to try to ensure that BOSS Music Player is bug free. Should you find a bug, please report what you find using the issue tracker found at:  [https://gitlab.com/smoothswim/boss/issues](https://gitlab.com/smoothswim/boss/issues).

When creating a report, it is helpful good practice to structure your post using something similar to the following outline:

* **The issue**
* **How to reproduce the issue**
* **The expected behaviour**
* **The actual behaviour**
* **Summary**

Please try to include any patches, links, merge requests or relevant information which help to fix or prove the issue you are reporting.

Submitting a good bug report increases the likelihood that your issue will be understood and fixed quickly.

_Please understand that the project's issue tracker is not a support forum and that it does not exist for that purpose. If in doubt, please refrain from asking questions when creating a new issue using the issue tracker, unless there is a relevant and technical (not personal) benefit to doing so. It goes without saying that you should also please try to ensure that you communicate in an appropriate and polite manner at all times, especially whilst trying to get your point across to project members. Please also completely avoid aggressive and abusive tones in your communication. Please stay on topic, avoid combative or polarising conversation and describe what you mean as best you can and to the best of your ability._

**Thanks!**
