#!/bin/bash
#
#
# Builds an android apk of the application
# (Current status is working but the produced app is very buggy)
# This is currently only for the 'x86_64' emulator
# The emulator must be running and be the default connected adb device
# for the apk to install

# Currently unused options
#-DNDROID_TOOLCHAIN='clang' \
#-DANDROID_ABI='arm64-v8a' \
#-DANDROID_BUILDTOOLS_REVISION='30.0.3' \

cd ..
if [ -d "build" ]; then rm -rf build; fi
mkdir build && cd build
cmake -DQt5_DIR=/build/qt-android/5.15.2/android/lib/cmake/Qt5 \
-DQt5Core_DIR=/build/qt-android/5.15.2/android/lib/cmake/Qt5Core \
-DQt5Widgets_DIR=/build/qt-android/5.15.2/android/lib/cmake/Qt5Widgets \
-DQt5Gui_DIR=/build/qt-android/5.15.2/android/lib/cmake/Qt5Gui \
-DQt5Multimedia_DIR=/build/qt-android/5.15.2/android/lib/cmake/Qt5Multimedia \
-DQt5Network_DIR=/build/qt-android/5.15.2/android/lib/cmake/Qt5Network \
-DQt5AndroidExtras_DIR=/build/qt-android/5.15.2/android/lib/cmake/Qt5AndroidExtras \
-DJAVA_HOME=/build/android-studio/jre \
-DANDROID_NDK=/build/android-sdk/ndk/22.0.7026061 \
-DANDROID_SDK=/build/android-sdk \
-DQT_ANDROID_SDK_ROOT=/build/android-sdk \
-DANDROID_NATIVE_API_LEVEL='29' \
-DANDROID_ABI='x86_64' \
-DANDROID_STL='c++_shared' \
-DCMAKE_TOOLCHAIN_FILE=/build/android-sdk/ndk/22.0.7026061/build/cmake/android.toolchain.cmake ..
make
if [ -f /build/boss/build/BOSS-x86_64/build/outputs/apk/release/BOSS-x86_64-release-signed.apk ]; then /build/android-sdk/platform-tools/adb install /build/boss/build/BOSS-x86_64/build/outputs/apk/release/BOSS-x86_64-release-signed.apk; fi
cd ..
cd android

exit
